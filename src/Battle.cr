require "./Actors.cr"
require "./Labels.cr"

module Battle 
  include Labels
  include Actors

  enum BState
    Ready
    Ticking
  end


  NoTick = Tick.new(TickType::EmptyTick,"", {} of String => AnyData)

  class BattleActorState
    property time     : SF::Time
    property b_state  : BState
    property tick_queue : Deque(Tick)
    #this is where the skill stats are stored
    property battle_stats : Hash(String, AnyData)
    property status       : Hash(String, AnyData)

    def initialize()
      @b_state = BState::Ready
      @time = SF.seconds(0)
      @tick_queue = Deque.new(0) { |i| Battle::NoTick }
      @battle_stats = {} of String => AnyData
      @status = {} of String => AnyData
    end

    def update(dt)
      
      case @b_state
      when  BState::Ready
        @time+=dt
      else
        @time-=dt
        if @time.as_seconds <= 0
          @b_state = BState::Ready
        end
      end

    end

    def get_tick()
      @tick_queue.shift?
    end
  end

  




end