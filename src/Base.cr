require "./Paradox/*"


TSZ = Paradox::TILESIZE
alias MenuCoord = {Coord,Coord}

alias Coord = {Int32, Int32}
module Base
  extend self


  alias MenuCoord = {Coord,Coord}

  alias AnyData = String | Float64 | Int64 | Metadata

  alias Metadata = Hash(String,AnyData)

  class ::String
    def includes_any?(input : String)
    all = input.map do |char| 
      self.includes?(c)
    end
    return !all.includes?(false)
    end
  end

  def pluc(a,b)
    x1,y1 = a
    x2,y2=b
    return {x1*y2+x2*y1,y1*y2} 
  end
  
  def center_text(txt)
    rect = txt.local_bounds
    txt.origin = { rect.left + rect.width / 2, rect.  top + rect.height / 2}
  end

  def center_sprite(sprite)
    tr = sprite.texture_rect
    w = tr.width
    h = tr.height
    sprite.origin = { w/2, h/2 }
  end

  def center_rect(rect)
    w = rect.size[0]
    h = rect.size[1]
    rect.origin = { w/2, h/2 }
  end

  def left_text(txt)
    rect = txt.local_bounds
    txt.origin = {0, rect.  top + rect.height / 2}
  end

  def get_scr_pos(config, mpos : MenuCoord)
    nx = mpos[0][0]
    dx = mpos[0][1]
    ny = mpos[1][0]
    dy = mpos[1][1]
    return ({config.size[0] * nx/dx, config.size[1] * ny/dy})
  end

  def getX(coord : MenuCoord)
    return coord[0][0].fdiv(coord[0][1])
  end

  def getY(coord : MenuCoord)
    return coord[1][0].fdiv(coord[1][1])
  end


  def get_distance(coord1, coord2)
    x1 = getX(coord1)
    y1 = getY(coord1)

    x2 = getX(coord2)
    y2 = getY(coord2)
    return Math.sqrt((x2-x1)**2+(y2-y1)**2)
  end

  def get_diff(coord1, coord2)
    x1 = coord1[0]
    y1 = coord1[1]

    x2 = coord2[0]
    y2 = coord2[1]
    return Math.sqrt((x2-x1)**2+(y2-y1)**2)
  end


  def convert_mouse(mvector,wsize)
    x = mvector.x
    y = mvector.y

    fx = {x,wsize[0]}
    fy = {y,wsize[1]}

    return {fx,fy}

  end



  def is_in_dir(coord1, coord2, direction)
    case direction
    when 8
      unless (getY(coord1) > getY(coord2))
        return false
      end
    when 2
      unless (getY(coord1) < getY(coord2))
        return false
      end
    when 6
      unless (getX(coord1) < getX(coord2))
        return false
      end
    when 4
      unless (getX(coord1) > getX(coord2))
        return false
      end
    end
    return true
  end

  def closest(pos,ar)
    arro = ar - [pos]
    return arro.min_by{|x| get_distance(x,pos)}
  end


  def move_dir(tup, dir)
    case dir
    when 2
      return {tup[0],tup[1]+1}
    when 8
      return {tup[0],tup[1]-1}
    when 4
      return {tup[0]-1,tup[1]}
    when 6
      return {tup[0]+1,tup[1]}
    else
      return tup
    end
  end

  def move_dir_inside(tup, dir,arr)
    rtup = {-1,-1}
    case dir
    when 2
      rtup = {tup[0],tup[1]+1}
    when 8
      rtup = {tup[0],tup[1]-1}
    when 4
      rtup = {tup[0]-1,tup[1]}
    when 6
      rtup =  {tup[0]+1,tup[1]}
    end
    if arr.includes? rtup  
      return rtup 
    else
      return tup
    end
  end


  def snap(coord)
    x,y = coord
    return {x.round.to_i,y.round.to_i}
  end

  def snap2(coord)
    x,y = coord
    return SF.vector2(x.round.to_i,y.round.to_i)
  end


  def v_from_angle_norm(angle,norm)
    x,y = Math.cos(angle)*norm, Math.sin(angle)*norm
    v = SF::Vector2.new(x,y)
    return v
  end

  def deg_to_rad(angle)
    return (angle-90)*0.0174533
  end

  def get_norm(vec)
    return Math.sqrt(vec.x**2 + vec.y**2)
  end

  def get_unitary(vec)
    return Base.with_norm(vec,1.0)
  end

  def with_angle(vec, angle)
    return Base.v_from_angle_norm(angle, Base.get_norm(vec))
  end

  def with_dir(vec1, vec2)
    ang = Base.get_angle(vec1) - Base.get_angle(vec2)
    fang = Base.get_angle(vec2)
    if ang.abs > Math::PI / 2
      return Base.with_angle(vec1, Math::PI + fang)
    else
      return Base.with_angle(vec1,fang)
    end
  end

  def cut_tileset(texture,pos)
      sprite = SF::Sprite.new(texture)
      sprite.texture_rect = SF.int_rect(pos[0]*TSZ,pos[1]*TSZ,TSZ,TSZ)
      return sprite
  end

  def flip_texture(texture)
    img = texture.copy_to_image
    img.flip_horizontally
    txt = SF::Texture.load_from_image(img)
  end

  def surname(object)
    return object.class.name.split("::").last
  end
end
