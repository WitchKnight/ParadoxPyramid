require "./Paradox/*"
require "./Game.cr"

# TODO: Write documentation for `Paradox`
module Paradox

  conf = Config::Config.new
  window  = SF::RenderWindow.new(SF::VideoMode.new(conf.size[0], conf.size[1]),style: SF::Style::Close, title: conf.name)
  window.vertical_sync_enabled = true

  game    = GState.new(window)
  clock = SF::Clock.new

  while window.open?
    dt = clock.restart

    game.update(dt)
    # check all the window's events that were triggered since the last iteration of the loop
    while event = window.poll_event
      game.handleEvents(event,window)
    end
    window.clear(SF::Color::Black)
    game.draw
    window.display()
  end
  
end
