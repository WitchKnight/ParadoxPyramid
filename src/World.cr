require "./Actors.cr"

require "../lib/crsfml/src/*"
require "../lib/crsfml/src/audio/*"



module World
  extend self

  enum Domain
    Castle
    Temple
    Cave

    def tileset
      texture = SF::Texture.new
      case self 
      when Castle
        texture = SF::Texture.from_file("images/tilesets/castle.png")
      when Temple
        texture = SF::Texture.from_file("images/tilesets/temple.png")
      when Cave
        texture = SF::Texture.from_file("images/tilesets/cave.png")
      end
      return texture
    end

    def background
      texture = SF::Texture.new
      case self
      when Castle
        texture = SF::Texture.from_file("images/backgrounds/test/background-1.png")
      when Temple
        texture = SF::Texture.from_file("images/backgrounds/test/background-11.png")
      when Cave
        texture = SF::Texture.from_file("images/backgrounds/test/background-16.png")
      end
      return texture
    end

  end

  enum Tile
    None
    Pillar1
    Pillar2
    Pillar3
    WallH
    Elem1
    Elem2
    Elem3
    WallV
    DoorH
    DoorV
    ODoorH
    ODoorV
    Trans1
    Trans2
    Ground1
    Ground2

    def pos
      y,x = {0,0}
      #why y,x instead of x,y ? why not ? true answer is that I made a mistake and then was too lazy to change
      case self
      when None
        y,x = {-1,-1}
        # dirty trick so that nothing is shown. Look @ Base.cut_tileset
      when Pillar1
        y,x =  {0,0}
      when Pillar2
        y,x =  {0,1}
      when Pillar3
        y,x =  {0,2}
      when WallH
        y,x =  {0,3}
      when Elem1
        y,x =  {1,0}
      when Elem2
        y,x =  {1,1}
      when Elem3
        y,x =  {1,2}
      when WallV
        y,x =  {1,3}
      when ODoorH
        y,x =  {2,0}
      when ODoorV
        y,x =  {2,1}
      when DoorV
        y,x =  {2,2}
      when DoorH
        y,x =  {2,3}
      when Trans1
        y,x =  {3,0}
      when Trans2
        y,x =  {3,1}
      when Ground1
        y,x =  {3,2}
      when Ground2
        y,x =  {3,3}
      end
      return {x,y}
    end

    def get_map_rec
      rec = SF::RectangleShape.new({5,5})
      rec.fill_color = SF::Color::White
      case self
      when Ground1
        res = rec
      when Ground2
        res = rec
      else
        res = SF::RectangleShape.new
      end
    end

    def move?
      case self
      when Ground1
        return true
      when Ground2
        return true
      else
        return false
      end
    end


  end

  def get_tile(int)
    return Tile.new(int)
  end

  class Layer
    property map      : Hash({Int32,Int32},Tile)
    property domain   : Domain
    property offset   : {Int32,Int32}
    property opacity  : Float64
    property blocking : Bool

    def initialize(@domain, @map)
      @opacity = 1.0
      @offset = {0,0}
      @blocking = false
    end

    def initialize(@domain, @map, @offset)
      @opacity = 1.0
      @blocking = false
    end

  end



  class Zone
    property name     : String
    property curlayer : Int32
    property start    : {Int32,Int32}
    property layers   : Array(Layer)
    property actors   : Actors::HActors
    property validmoves : Array({Int32,Int32})
    property display  : Array(SF::Sprite)
    property display_mini : Array(SF::RectangleShape)
    # property items  : Array(Items::Item)

    def initialize(@layers,@actors,@start,@name)
      @curlayer = -1
      @items = 0
      @validmoves = current_layer.map.select{|k,v|v.move?}.each_key.to_a 
      @display = gen_map
      @display_mini = gen_mini_map
    end

    def get_layer
      nb = layers.size
      case curlayer
      when 0
        return "RDC"
      when -1
        return "top floor"
      when (nb-1)
        return "top floor"
      else
        return "floor " + curlayer.to_s
      end
    end

    def move?(coord)
      return validmoves.includes? coord
    end

    def current_layer
      @layers[curlayer]
    end



    def gen_map
      gmap = [] of SF::Sprite
      @layers.each { |layer|
        layer.map.each { |tpos, tile|
          c = Base.cut_tileset(layer.domain.tileset,tile.pos)
          Base.center_sprite(c)
          c.position = {tpos[0]*TSZ,tpos[1]*TSZ}
          gmap.push(c)
        }
      }
      return gmap
    end

    def gen_mini_map
      gmap = [] of SF::RectangleShape
      current_layer.map.each { |tpos,tile|
        c = tile.get_map_rec
        Base.center_sprite(c)
        c.position = {tpos[0]*5,tpos[1]*5}
        gmap.push(c)
      }
      return gmap
    end
  
  end


  class BattleZone
    property background : SF::Sprite
    property zone_type  : Domain

    def initialize(@zone_type)
      @background = SF::Sprite.new(@zone_type.background)
      Base.center_sprite(@background)
      @background.scale = SF.vector2(4.0,4.0)
    end
  end


  

end
