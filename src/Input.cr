
require "../lib/crsfml/src/*"
require "../lib/crsfml/src/audio/*"
require "yaml"

module Input
  extend self
  include Base
  
  
 
  enum Uni_moves
    Confirm
    Cancel
    Nothing
  end

  enum Menu_moves
    Up
    Left
    Right
    Down
  end

  enum Map_moves
    Up
    Left
    Right
    Down
    Battle
  end

  alias GPAction = Uni_moves | Menu_moves | Map_moves

  def ac_from_keys(k,k2)
    case k
    when "uni_moves"
      return Uni_moves.parse(k2.capitalize)
    when "menu_moves"
      return Menu_moves.parse(k2.capitalize)
    when "map_moves"
      return Map_moves.parse(k2.capitalize)
    else
      return Uni_moves.parse("Nothing")
    end


  end




  alias EKT = Tuple(Array(SF::Keyboard::Key),String, String)


  class GInputs
    property akmap  : Hash(GPAction,EKT)

    def initialize
      @akmap = {} of GPAction => EKT
      reload
    end

    def load(inputs)
      @akmap.clear
      inputs.each{ |k,v| 
        v.each { |k2,v2|
          @akmap[Input.ac_from_keys(k,k2)] =  {v2.map { |x| SF::Keyboard::Key.parse(x)}, k, k2}
        }
      }
    end

    def export
      a = {} of String => Hash(String, Array(String))
      a["uni_moves"] = {} of String => Array(String)
      a["menu_moves"] = {} of String => Array(String)
      a["map_moves"] = {} of String => Array(String)
      akmap.each { |action,v|
        a[v[1]][v[2]] = v[0].map {|key| key.to_s}
      }
      return a
    end

    def reload
      raw = File.read("config/keys.yaml")
      inputs = Hash(String, Hash(String, Array(String))).from_yaml  raw
      load(inputs)
      p "reloading"
      p self
    end

    def add_binding(k,k2)
      new_inputs = export
      (0..100).each{ |i|
        key = SF::Keyboard::Key.new(i)
        if SF::Keyboard.key_pressed? key
          new_inputs[k][k2] << key.to_s
        end
      }
      File.open("config/keys.yaml", "w") { |f| new_inputs.to_yaml(f); f.close}
      reload
    end

    def remove_binding(k,k2,key)
      new_inputs = export
      new_inputs[k][k2]-=[key]
      File.open("config/keys.yaml", "w") { |f| new_inputs.to_yaml(f); f.close }
      reload
    end

    def trigg(action, input)
      akmap[action][0].includes? input
    end

    def isPressed(action)
      akmap[action][0].map{|k| SF::Keyboard.key_pressed? k}.any?
    end

    def get_menu_dir
      a = [] of Int32
      a << 8 if isPressed(Menu_moves::Up) 
      a << 4 if isPressed(Menu_moves::Left)
      a << 6 if isPressed(Menu_moves::Right)
      a << 2 if isPressed(Menu_moves::Down)
      return a
    end

    def get_map_dir
      a = [] of Int32
      a << 8 if isPressed(Map_moves::Up) 
      a << 4 if isPressed(Map_moves::Left)
      a << 6 if isPressed(Map_moves::Right)
      a << 2 if isPressed(Map_moves::Down)
      return a
    end

  end
  
  gi = GInputs.new
  p gi

  class Cooldowns
    property data   : Hash(GPAction, Float64) = {} of GPAction => Float64
    property inputs : GInputs
    def initialize(@inputs)
      Uni_moves.each { |e| 
        @data[e] = 0.0
      }
      Menu_moves.each { |e| 
        @data[e] = 0.0
      }
      Map_moves.each { |e| 
        @data[e] = 0.0
      }
    end

    def trigger(action,time)
      if @data[action] <= 0.01
        if inputs.isPressed action
          @data[action]=time 
          return true
        end
      else
        return false
      end
    end

    def update(dt)
      @data.each{|k,v| 
        @data[k]=(v - dt.as_seconds).clamp(0.0, 10.0) 
      }
    end
  end




end