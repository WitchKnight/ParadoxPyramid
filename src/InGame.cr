require "./Scenes.cr"

module InGame
  include Scenes




  class IGMenuScene < MenuScene
    property roam_scene : RoamScene
    property builder    : Proc(RoamScene, Menu)

    def initialize(menu_builder, roam_scene, game)
      @builder = menu_builder 
      menux = builder.call(roam_scene) 
      super(menux,game)
      @roam_scene = roam_scene
    end

    def rebuild
      @selector = Base.closest(@selector,@menu.elements.each_key.to_a)
      @menu = builder.call(@roam_scene)
    end


  end

end