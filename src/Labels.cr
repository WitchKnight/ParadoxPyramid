require "./AllLabels.cr"
require "./Animation.cr"

module Labels
  extend self
  include AllLabels
  include Animation
  include Base

  abstract class Label

    property power : Int32 = 0

    def initialize
      @power = 0
    end

    def initialize(@power)
    end
    
    def label_name  
      return Base.surname self
    end

    def to_s
      return label_name + " lv " + power.to_s
    end 

    def label_texture
      return None.new(0).label_texture
    end
  end

  macro define_labels
    {% for name in AllLabels %}
      class {{name.id}} < Label
        Texture{{name.id}}= begin
          begin
            a= SF::Texture.from_file("images/labels/{{name.id}}.png")
          rescue 
            a= None.new(0).label_texture
            #MAKE SURE THAT THERE IS A NONE PIC. IF THERE IS NOT IT WILL BREAK EVERYTHING
          end
          a
        end
        def label_sprite
          a = Texture{{name.id}}
          sprite = SF::Sprite.new(a)
          Base.center_sprite(sprite)
          return sprite
        end
        def label_texture
          return Texture{{name.id}}
        end
      end
    {% end %}

    def to_label(str,pow)
      case str.capitalize
      {% for name in AllLabels %}
      when "{{name.id}}"
        {{name.id}}.new pow
      {% end %}
      else
        None.new pow
      end
    end

  end

 
  
  macro union_labels
    alias LabelU = 
      {% for name in AllLabels %}
        {{name.id}} | 
      {% end %}
      None
  end
  

  define_labels 
  union_labels
  
  class Item < Label
    property name         : String
    property qualities    : Array(LabelU)
    # property usable       : Usable
    property weight       : Int32
    property description  : String
    property anim         : SpriteAnim

    def initialize(power,@name,@qualities,@weight,@description,@anim)
      super(power)
    end

    def get_anim(time)
      @anim.get_cur(time)
    end 

    def to_s
      a = [
        "Item",
        "name : " + name,
        "usable : " +(!usable.is_a? Nil).to_s,
        "weight : " + weight.to_s,
        "qualities : " +qualities.map{|x|x.to_s}.join(" ") 
      ].join("\n")
      return a
    end  

    def label_sprite
      if qualities.any?
        return qualities[0].label_sprite
      else
        return None.new(0).label_sprite
      end
    end

  end

  class Weapon < Item
    property wp_type    : Array(LabelU)
    property damage     : Int32
    property parade     : Int32
    property crit       : Int32
    property speed      : Int32
    property range      : Int32 
    def initialize(power, name, qualities, weight, description, anim, @wp_type, skills, @damage,@parade,@crit,@speed,@range)
      super(power, name, qualities, weight, description, anim)
    end
    def to_s
      a = "Weapon : " + wp_type.map{|x|x.label_name + " lv " + x.power.to_s}.join(" ")
    end
    def label_sprite
      if wp_type.any?
        return wp_type[0].label_sprite
      else
        return None.new(0).label_sprite
      end
    end
  end

  class Armor < Item
    property arm_type   : Array(LabelU)
    property materials  : Array(LabelU)
    property coverage   : Array(LabelU)
    property defense    : Int32
    
    def initialize(power, name, qualities, weight, description, anim, @arm_type,skills, @materials, @coverage, @defense)
      super(power, name, qualities, weight, description, anim)
    end

    def label_sprite
      if arm_type.any?
        return arm_type[0].label_sprite
      else
        return None.new(0).label_sprite
      end
    end

    def to_s
      a = "Armor: \ntypes: " + arm_type.map{|x|x.label_name + " lv " + x.power.to_s}.join(" ") + "\nmaterials: " + materials.map{|x|x.label_name + " lv " + x.power.to_s}.join(" ") 
    end
  end

  LightArmor= ->(k: Label){
    k.is_a? Leather
  }

  HeavyArmor= ->(k: Label){
    k.is_a? Iron
  }

  def getPw(lbs,condition)
    sum = 0
    lbs.each { |k|
      c = k.power
      if condition.call(k)
        sum += c if c.is_a? Int32
      end
    }
    return sum
  end
  
  def getNb(lbs,condition)
    return lbs.select{|el|condition.call(el)}.size
  end

  def wields(lbs,condition)
    return getNb(lbs, ->(k : Label){k.is_a? Weapon && k.wp_type.select{|typ| condition.call(typ)}.any? })
  end

  def wears(lbs,condition)
    return getNb(lbs, ->(k : Label){k.is_a? Armor && k.materials.select{|typ| condition.call(typ)}.any? })
  end

  GenderParadox = -> (lbs : Array(Label)){
    fpw     = getPw(lbs,->(k: Label){k.is_a? Female})
    mpw     = getPw(lbs,->(k: Label){k.is_a? Male})
    maxG    = [mpw,fpw].max
    minG    = [mpw,fpw].min
    center  = mpw - fpw
    tG      = mpw + fpw
    paradox = ((minG**2).fdiv(1 + center.abs)).round(1)
    return {paradox,maxG,minG,center,tG}
  }

  WeaponParadox = -> (lbs : Array(Label)){
    nweap = getNb(lbs,->(k: Label){ k.is_a? Weapon })
    ambidextry = getPw(lbs, ->(k: Label){ k.is_a? Ambidextry})
    paradox = (nweap - 1).clamp(0,100).fdiv(1 + ambidextry/5) ** 2
    return {paradox,nweap,ambidextry}
  }

  AllParadox = [GenderParadox,WeaponParadox]

  def total_paradox(lbs)
    return AllParadox.map { |func| (func.call lbs)[0]}.sum
  end
  


end
