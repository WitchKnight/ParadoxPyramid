module Config

  class Config
    property volume : {Int32,Int32} = {5,5}
    property size   : {Int32,Int32} = {800,600}
    property name   : String        = "Paradox"
  
    def initialize
    end
    def initialize(@volume, @size)
    end

    def center
      return SF.vector2(size[0]/2, size[1]/2)
    end
  end
  
end
