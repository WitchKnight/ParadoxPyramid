module Menus 
  extend self

  include Events
  include Actors
  include Scenes
  include InGame
  include Sound
  include Base
  
  SelecRec = SF::Texture.from_file("images/menus/selec_rec.png")
  ULineT = SF::Texture.from_file("images/menus/underline.png")
  def plain_text(text,size)
    SMElem.new(
      mk_std_label(text,size),
      false,
      nil,
      nil)
  end

  def color_text(text,size,color)
    SMElem.new(
      mk_std_label(text,size,color),
      false,
      nil,
      nil)
  end

  def color_text_on_action(text,size,color,action)
    SMElem.new(
      mk_std_label(text,size,color),
      true,
      nil,
      nil,
      action)
  end

  def text_on_action(text,size,action)
    SMElem.new(
      mk_std_label(text,size),
      true,
      nil,
      nil,
      action)
  end
  
  def plain_pics(pics)
    SMElem.new(
      BlankLabel,
      false,
      ->(t: SF::Time){
      return pics
      },
      nil,
      nil
    )
  end

  def plain_pic(pic)
    plain_pics([pic])
  end

  def selec_pic(pic)
    selec_pics([pic], 0.5)
  end

  def selec_pics(pics, osize)
    r = SF::Sprite.new(SelecRec)
    center_sprite(r)
    return SMElem.new(
      BlankLabel,
      true,
      ->(t: SF::Time){
      return pics
      },
      ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = osize * (1.0 - t2.fdiv(40))
          cr = r
          cr.scale = SF.vector2(size,size)
          return pics + [cr]
        },
      nil,
      nil
    )
  end


  def selec_text_on_action(text,size,color,action)
    r = SF::Sprite.new(SelecRec)
    center_sprite(r)
    SMElem.new(
      mk_std_label(text,size,color),
      true,
      nil,
      ->(t : SF::Time){
        t2 = (t.as_seconds*10).to_i % 3 - 1
        size =  0.5 * (1.0 - t2.fdiv(40))
        cr = r
        cr.scale = SF.vector2(size,size)
        return [cr]
      },
      nil,
      action)
  end

  def uline_text_on_action(text,size,color,action)
    r = SF::Sprite.new(ULineT)
    center_sprite(r)
    SMElem.new(
      mk_std_label(text,size,color),
      true,
      nil,
      ->(t : SF::Time){
        t2 = (t.as_seconds*10).to_i % 3 - 1
        size =  0.5 * (1.0 - t2.fdiv(40))
        cr = r
        cr.scale = SF.vector2(size,size)
        return [cr]
      },
      nil,
      action)
  end

  def selec_pics_on_action(pics, osize, action)
    r = SF::Sprite.new(SelecRec)
    center_sprite(r)
    return SMElem.new(
      BlankLabel,
      true,
      ->(t: SF::Time){
      return pics
      },
      ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = osize * (1.0 - t2.fdiv(40))
          cr = r
          cr.scale = SF.vector2(size,size)
          return pics + [cr]
        },
      nil,
      action
    )
  end

  def uline_pics_on_action(pics, osize, action)
    r = SF::Sprite.new(ULineT)
    center_sprite(r)
    return SMElem.new(
      BlankLabel,
      true,
      ->(t: SF::Time){
      return pics
      },
      ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = osize * (1.0 - t2.fdiv(40))
          cr = r
          cr.scale = SF.vector2(size,size)
          return pics + [cr]
        },
      nil,
      action
    )
  end

  









  BuildIGMenu = ->(s : RoamScene) {
    act = s.rplay
    acts = [act] + s.npcs.map {|n| n.to_act} 

    r = SF::Sprite.new(SelecRec)
    center_sprite(r)
    r.position = {-60,0}

    elems = {} of MenuCoord => MenuElem
   
    elems[{ {1,10},{9,10} }]= text_on_action("Back",25,->(game: GState) {game.switch_scene(s)}).cancel

    pos ={ {1,12}, {1,10} }
    elems[pos] = plain_text(s.zone.name + ", " +s.zone.get_layer,42)
    acts.each_with_index do |act,inc|
      acolor = act.faction.color
      i = inc*2 + 1
      tags = act.build_identity
      cls  = act.classes
      pos1 ={ {1,12}, {i+1,9} }
      elems[pos1] = color_text(act.name, 40, acolor)

      pos2 = { {1,8}, {i+2,9} }
      elems[pos2] = SMElem.new(
        BlankLabel,
        true,
        ->(t : SF::Time){[act.get_anim_p(s.time),r]},
        ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = 1.0 - t2.fdiv(40)
          cr = r.dup
          cr.scale = SF.vector2(size,size)
          return [act.get_anim_p(s.time + t),cr]
        },
        nil,
        ->(game: GState){
          game.switch_ig_menu(build_status_menu(game,act),s)
        }
      )

      tags.each_with_index { |c,j|
        pos = { {2, 10}, pluc({1+i,9},{1+j,18})  }
        elems[pos] = plain_text(c,25) 
      }
    
      cls.select{|c|c.active}.each_with_index { |c,j|
        pos = { {4, 10}, pluc({1+i,9},{1+j,18})  }
        elems[pos] = plain_text(c.name + " lv " + c.level.to_s,25)
      }

      pos = { {2,10}, {i+1,9} }
      elems[pos] = plain_text("HP : "+ act.stats[1].to_s,24)

      pos = { {3,10}, {i+1,9} }
      elems[pos] = plain_text("MP : "+ act.stats[2].to_s,24)

      pos = { {4,10}, {i+1,9} }
      elems[pos] = plain_text("PDX : "+ act.get_paradox.to_s,24)

    end
    
    elems[{ {10,12},{2,8} }] = text_on_action("Attributes",32, 
      nil
    )

    elems[{ {10,12},{3,8} }] = text_on_action("Equipment",32,
      ->(game: GState){game.switch_ig_menu(build_equip_menu,s)}
    )

    elems[{ {10,12},{4,8} }] = text_on_action("Items",32,
      nil
    )

    elems[{ {10,12},{6,8} }] = text_on_action("Skills",32,
      nil
    )

    elems[{ {10,12},{5,8} }] = text_on_action("Titles",32,
      nil
    )

    elems[{ {10,12},{7,8} }] = text_on_action("Main Menu",32,
      ->(game: GState){game.switch_menu(Init::MainMenu)}
    )

    return Menu.new(elems, { {10,12}, {2,8} } )

  }



  def build_equip_menu
    buildEquipMenu = ->(s: RoamScene){
      r = SF::Sprite.new(SelecRec)
      center_sprite(r)
      act = s.rplay
      eitems = act.items
      pitems = s.player.inventory.select{|k,x|!eitems.includes? x}
      r = SF::Sprite.new(SelecRec)
      center_sprite(r)
      r.position = {-60,0}

      elems = {} of MenuCoord => MenuElem
   
      elems[{ {1,10},{9,10} }]= text_on_action("Back",25,
        ->(game: GState) {game.switch_ig_menu(BuildIGMenu,s)}
      ).cancel

      pos = { {1,10}, {4,9} }
      elems[pos] = plain_text("PDX : "+ act.get_paradox.to_s,24)

      pos1 ={ {1,12}, {1,9} }
      elems[pos1] =  elems[pos1] = color_text(act.name, 40, color: SF::Color::Green)
      pos2 = { {1,8}, {2,9} }
      elems[pos2] = SMElem.new(
        BlankLabel,
        false,
        ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = 1.0 - t2.fdiv(40)
          cr = r.dup
          cr.scale = SF.vector2(size,size)
          return [act.get_anim_p(s.time + t),cr]
        },
        nil,
        nil
      )

      elems[{ {3,10}, {1,9}}] = plain_text("Equipped",40)
      
      eitems.each_with_index{ |item,i|
        pos = { {3,10}, pluc({1,9},{i+1,18}) }
        anim = item.anim
        elems[pos] = SMElem.new(
          BlankLabel,
          true,
          ->(t: SF::Time){
            return [item.get_anim(t)]
          },
          ->(t : SF::Time){
            t2 = (t.as_seconds*10).to_i % 3 - 1
            size = 0.5 - t2.fdiv(40)
            cr = r.dup
            cr.scale = SF.vector2(size,size)
            return [item.get_anim(t),cr]
          },
          nil,
          ->(game: GState){
            act.unequip(item)
            game.refresh
            nil
          }
          ).threshold(0.015)
      }
      elems[{ {3,10}, {4,9}}] = plain_text("Available",40)

      pitems.each{ |ipos,item|
        x,y = ipos
        pos = { pluc({3,10},{x,18}), pluc({4,9},{y+1,18}) }
        
        elems[pos] = SMElem.new(
          BlankLabel,
          true,
          ->(t: SF::Time){
            pic = item.get_anim(t)
            pic.scale={2,2}
            return [pic]
          },
          ->(t : SF::Time){
            pic = item.get_anim(t)
            pic.scale={2,2}
            t2 = (t.as_seconds*10).to_i % 3 - 1
            size = 0.5 - t2.fdiv(40)
            cr = r.dup
            cr.scale = SF.vector2(size,size)
            return [pic,cr]
          },
          nil,
          ->(game: GState){
            act.equip(item)
            game.refresh
            nil
          }

        ).threshold(0.015)
      }
      
      equipExtras = -> (c : MenuCoord){
        pitems.each{ |ipos,item|
          x,y = ipos
          pos = { pluc({3,10},{x,18}), pluc({4,9},{y+1,18}) }
          if pos == c

            pos1 = { {3,10},{7,9} }
            elem= plain_text(display_description(item.description),25)
            
            pos2 = { {3,10},{6,9} }
            elem2 =  plain_text(item.name,24)
            
            pic = item.get_anim(SF.seconds(0)).dup
            pic.scale = {2,2}            
            pos3 = { {2,10},{6,9} }
            elem3 =  plain_pic(pic)

            pos4 =  { {13,18},{13,18} }
            elem4 =plain_text("weight: " + item.weight.to_s, 23)

            ielems = { pos1 => elem, pos2 => elem2, pos3 => elem3, pos4 => elem4}

            cur = 0
            item.qualities.each{ |l|
              if l.is_a? Weapon
                l.wp_type.each_with_index { |l,i|
                  ielems[{ pluc({3,10},{i,20}),{25,36}}] =  plain_text(l.label_name+ " lv " + l.power.to_s,20)
                }
                
                ielems[{ {13,18},pluc({cur+1,36},{13,18}) }] =  plain_text("DMG: " +l.damage.to_s,23)
                ielems[{ {13,18},pluc({cur+2,36},{13,18}) }] =  plain_text("CRT: " +l.crit.to_s,23)                
                ielems[{ {13,18},pluc({cur+3,36},{13,18}) }] =  plain_text("SPD: " +l.speed.to_s,23)
                ielems[{ {13,18},pluc({cur+4,36},{13,18}) }] =  plain_text("RNG: " +l.range.to_s,23)
                ielems[{ {13,18},pluc({cur+5,36},{13,18}) }] =  plain_text("BLK: " +l.parade.to_s,23)
              elsif l.is_a? Armor
                l.arm_type.each_with_index { |l,i|
                  ielems[{ pluc({3,10},{i,20}),{25,36}}] =  plain_text(l.label_name + " lv " + l.power.to_s,20)
                }
                l.materials.each_with_index { |l,i|
                  ielems[{ pluc({3,10},{i,20}),{26,36}}] =  plain_text(l.label_name,20)
                }
                ielems[{ {13,18},pluc({cur+1,36},{13,18}) }] =  plain_text("DEF: " +l.defense.to_s,23) 
              end
            }

            return ielems
            break
          end
        }
        return {} of MenuCoord => MenuElem
      }
      return Menu.new(elems, { {1,10}, {9,10} }, equipExtras )
    }
  end

  def build_status_menu(game,act)
    soundTable = StandardSoundTable
    buildEquipMenu = ->(s: RoamScene){
      r = SF::Sprite.new(SelecRec)
      center_sprite(r)

      elems = {} of MenuCoord => MenuElem
   
      elems[{ {1,10},{9,10} }]= text_on_action("Back",25,
        ->(game: GState) {game.switch_ig_menu(BuildIGMenu,s)}
      ).cancel

      pos1 ={ {1,12}, {1,9} }
      elems[pos1] = color_text(act.name, 40, color: SF::Color::Green)

      pos2 = { {1,8}, {2,9} }
      elems[pos2] = SMElem.new(
        BlankLabel,
        false,
        ->(t : SF::Time){
          t2 = (t.as_seconds*10).to_i % 3 - 1
          size = 1.0 - t2.fdiv(40)
          cr = r.dup
          cr.scale = SF.vector2(size,size)
          return [act.get_anim_p(s.time + t),cr]
        },
        nil,
        nil,
        nil
      )

      weapons = act.labels.select{|x|x.is_a? Weapon}

      pos = { {1,10}, {4,9} }
      elems[pos] = plain_text("PDX : "+ act.get_paradox.to_s,24)

      elems[{ {2,8}, {1,9}}] = plain_text("Passives",40)

      act.labels.each_with_index { |lab,i|
        x  =  pluc({2,8}, { i % 4 , 18})
        y   = pluc({1,8}, { i // 4 + 1, 18})
        pos = {x,y}

        pic = lab.label_sprite
        Base.center_sprite(pic)
        pic.scale = {2,2}

        elems[pos]  = selec_pic(pic).threshold(0.03)
      }
      
      elems[{ {5,8}, {1,9}}] = plain_text("Classes",40)

      act.classes.each_with_index { |cls,i|
          x  =  {9,16}
          y   = pluc({1,8}, { i + 1, 18})
          pos = {x,y}

          pic = cls.cl_view.idleLoop.get_cur(SF.seconds(0))
          Base.center_sprite(pic)
          pic.scale = {2,2}  
          pic.color = SF::Color::Red if !cls.active
          p "n" if !cls.active
          
          toggleclass = ->(game: GState) {
            act.toggle_class(i)
            act.confirm
            game.refresh
            return nil
          }


          pos2 = {pluc({1,18},x),  y}

          elems[pos]  = selec_pics_on_action([pic], 0.5, toggleclass).threshold(0.03)
          elems[pos2] = plain_text("lv " + cls.level.to_s + " " +cls.name ,24) 
          elems[pos2] = color_text("lv " + cls.level.to_s + " " +cls.name ,24,SF::Color::Red)  if !cls.active
    }


      status_extra = -> (c : MenuCoord){

        xelems = {} of MenuCoord => MenuElem

        act.labels.each_with_index{ |lab,i|
          x  =  pluc({2,8}, { i % 4, 18})
          y   = pluc({1,8}, { i // 4 + 1, 18})
          pos = {x,y}
          if pos == c
            mpos =  { { 1,8}, {5,8}} 
            xelems[mpos] = color_text("lv " + lab.power.to_s + " " + lab.label_name,42,SF::Color::Blue)
          end
        }

        act.classes.each_with_index { |cls,i|
          x  =  pluc({2,8}, { i % 3 + 1, 18})
          y   = pluc({1,8}, { i // 3 + 1, 18})
          pos = {x,y}
         
        }

        return xelems
      }

      return Menu.new(elems, { {1,10}, {9,10} }, status_extra)
    }
  end


  
end