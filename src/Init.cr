require "./Scenes.cr"
require "./Menus.cr"
require "./ItemInit.cr"
require "json"


module Init
  extend self

  include Events
  include Actors
  include Scenes
  include Menus
  include ItemInit
  include Base

  AllSounds = {
    "confirm0" => SF::SoundBuffer.from_file("Sound/Confirm.ogg"),
    "cancel0" => SF::SoundBuffer.from_file("Sound/Cancel.ogg"),
    "step0" => SF::SoundBuffer.from_file("Sound/Step.wav"),
  }


  MainMenuElems = { 
    { {5,11}, {1,10} } => plain_text("Paradox",40),
    { {1,14}, {2,6} } => text_on_action("Commencer",25,
      ->(g: GState){ g.reset_scene(GetIntro.call(g))}
    ),
    { {1,14}, {3,6} } => text_on_action("Options",25,
      ->(game: GState){game.switch_menu(OptionsMenu)}),
    { {1,14}, {4,6} } => text_on_action("Quitter",25,
      ->(game: GState){game.quit}
    ).cancel,
    { {4,8}, {4,5} } => text_on_action("Test",25,
      ->(){puts "yeah"}
    )
  }
  MainMenu = Menu.new(MainMenuElems,{ {1,14}, {2,6} })

  OptionMenuElems = { 
    { {1,14}, {2,6} } => text_on_action("Graphics",25,
      ->(){puts "to be done"}
    ),
    { {1,14}, {3,6} } => text_on_action("Audio",25,
      ->(){puts "to be done"}
    ),

    { {1,14}, {4,6} } => text_on_action("Key Bindings",25,
      ->(game: GState){game.switch_menu(make_keys_menu(game))}
    ),

    { {1,14}, {5,6} } =>text_on_action("Back",25,
      ->(game: GState) {game.switch_menu(MainMenu)} 
    ).cancel
  }
  OptionsMenu = Menu.new(OptionMenuElems,{ {1,14}, {2,6} })


  def make_keys_menu(ogame)
    elems = {
        { {1,14}, {8,9} } => color_text("Hold the key(s) you want to bind before clicking the + symbol.", 25, SF::Color::Red),
        { {1,14}, {5,6} } => text_on_action("Back",25,
          ->(game: GState) {game.switch_menu(MainMenu)} 
        )
    }

    ogame.inputs.export.keys.each_with_index{ |k,i|
      j=0
      elems[ { {i+1, 5}, {1,14} } ] =  plain_text(k,30) 
      ogame.inputs.export[k].each_key { |k2|
        vertpos = {j+2, 11} 
        elems[ { {i+1,5},vertpos} ] = plain_text(k2,22)
        elems[ { pluc({i+1,5},{1,10}),vertpos } ] = selec_text_on_action("+",40,SF::Color::Blue,
          ->(game: GState){
            game.inputs.add_binding(k,k2)
            game.switch_menu(make_keys_menu(game))
          }
        ).threshold(0.02).centered
          
        ogame.inputs.export[k][k2].each_with_index { |key,n|
          elems[ { pluc({i+1,5},{n,20})  ,pluc(vertpos,{1,30}) } ] = uline_text_on_action(key,20,SF::Color::Green,
            ->(game: GState){
              game.inputs.remove_binding(k,k2,key)
              game.switch_menu(make_keys_menu(game))
            }
          ).threshold(0.02).centered
        }
        j+=1
      }
    }

    keyMenu = Menu.new(elems,{ {1,14}, {5,6} })
  end

  IntroFirst = ->(a: {Config::Config,SF::RenderWindow,SF::Time}){
    config,window,time = a
    text = Ft::MenuFont.get_text("It works ! " + time.as_seconds.to_s)
    center_text(text)
    text.position = get_scr_pos(config,{ {1,2},{1,2} })
    window.draw(text)
  }

  IntroSecond = ->(a: {Config::Config,SF::RenderWindow,SF::Time}){
    config,window,time = a
    h = "Paradox"
    i = time.as_seconds.to_i
    text = Ft::MenuFont.get_text(h[0..i])
    text.character_size = i*4 +24 
    center_text(text) 
    text.position = get_scr_pos(config,{ {1,2},{1,2} })
    window.draw(text)
  }

  GetIntro = ->(g: GState){CineScene.new({9.0 => IntroSecond},GetRoam.call(g),g)}


  ZeroScene = ->(){Scene.new()}

  TestZone = World::Zone.new(
    [
    World::Layer.new(
      World::Domain::Cave,
      {
        {0,0} => World::Tile::Ground1,
        {1,0} => World::Tile::Pillar1,
        {0,1} => World::Tile::Pillar2
      }
    )
    ], 
    {} of Int32 => Actor
  )
  
  
  TestPlayer  = NewPlayer.call("Newt")


 
  TestNPCs = [TestNPC1.call]

  GetRoam = ->(g: GState) {
    RoamScene.new(GetMap.call("maps/deb.json"),TestPlayer,TestNPCs,g)
  }
  GetMap = ->(file: String){
    jmap = JSON.parse(File.read(file))
    name = jmap["properties"]["name"].as_s.to_s
    sx = jmap["properties"]["startx"].as_i.to_i32
    sy = jmap["properties"]["starty"].as_i.to_i32
    height  = jmap["height"].as_i.to_i32
    width   = jmap["width"].as_i.to_i32
    mapdata = [] of Array(World::Tile)
    offsets = [] of {Int32,Int32}
    domains = [] of World::Domain 
    jmap["layers"].as_a.each do |layer|
      offsets << {layer["x"].as_i.to_i32,layer["y"].as_i.to_i32}
      domains << World::Domain.new (layer["properties"]["Domain"].as_i.to_i32)
      laydata = [] of World::Tile
      layer["data"].as_a.each do |data|
        x = World.get_tile(data.as_i.to_i32)
        laydata.push(x)
      end
      mapdata.push(laydata)
    end
    offsets.reverse!
    domains.reverse!
    layers = [] of World::Layer
    mapdata.each do |laydata|
      laymap = {} of {Int32,Int32} => World::Tile
      i = 0
      (0...height).each do |y|
        (0...width).each do |x|
          laymap[{x,y}]= laydata[i]
          i+=1
        end
      end
      layer = World::Layer.new(domains.pop, laymap, offsets.pop)
      layers << layer
    end 
    zone = World::Zone.new(
      layers, 
      {} of Int32 => Actor,
      {sx,sy},
      name
    )
    return zone
  }

  SageIdle    = SF::Texture.from_file("images/actors/sage.png")
  SageUp      = SF::Texture.from_file("images/actors/sageup.png")
  SageRi      = SF::Texture.from_file("images/actors/sageright.png")
  BowieId     = SF::Texture.from_file("images/actors/sage.png")
  BowieUp     = SF::Texture.from_file("images/actors/bowieback.png")
  BowieRi     = SF::Texture.from_file("images/actors/bowieright.png")
  SgPortrait  = SF::Sprite.new(SF::Texture.from_file("images/actors/portraits/sage.png"))
  SgAngry     = SF::Sprite.new(SF::Texture.from_file("images/actors/portraits/sageangry.png"))
  SgWonder    = SF::Sprite.new(SF::Texture.from_file("images/actors/portraits/sagewonder.png"))


  def sim_portrait(sprite)
    c = ->(a: Actor){
      return sprite
    }
  end

  def reg_emo(emosprites)
    sp = SF::Sprite.new
    c = ->(a: Actor){
      emosprites.each{ |emotion, sprite|
        sp = sprite if a.emotion == emotion
        center_sprite(sp)
      }
      return sp
    }
  end






  NewPlayer = ->(name: String){
    p_name = name
    
    st_anim = ->(texture: SF::Texture, arr: Array(Int32)){
      SpriteAnim.new(
        texture,
        arr
      )
    }

    a_view = ActorView.new(
      st_anim.call(SageIdle,[20,20,20,20]),
      st_anim.call(SageRi,  [20,20,20,20]),
      st_anim.call(SageUp,  [20,20,20,20]),
      sim_portrait(SgPortrait)
    )
    
    p_actor = Actor.new(
      p_name,
      a_view,
      Faction::Player
    )

    p = Player::Player.new(
      p_name,
      p_actor
    )

    Weapons.each_with_index{ |w,i|
      p.inventory[{i//4,i%4}] = w
    }

    Armors.each_with_index{ |a,i|
      j = Weapons.size + i
      p.inventory[{j//4,j%4}] = a
    }
    return p
  }

  TestNPC1    = ->(){
    
    st_anim = ->(texture: SF::Texture, arr: Array(Int32)){
      SpriteAnim.new(
        texture,
        arr
      )
    }

    a_view = ActorView.new(
      st_anim.call(SageIdle,[18,18,18,18]),
      st_anim.call(SageRi,  [18,18,18,18]),
      st_anim.call(SageUp,  [18,18,18,18]),
      reg_emo([
        {ActorEmotion::Neutral, SgPortrait},
        {ActorEmotion::Wonder,  SgWonder},
        {ActorEmotion::Angry,   SgAngry}
        ]
      )
    )

    c1 =cont_with(
      emo(ActorEmotion::Angry), 
      ev_dialog("How dare you. Brace yourself !",Events::Provoke_Battle)
    )
    
    c2 = cont_dialog(
      opt_dialog("Wohoo.. Wait then what am I ?",
        ["Tanga","Just you"],
        [
          just_text(["What","The","Fuck","Dude..."]),
          sim_dialog("That's very nice.")
        ])
    )
    
    dialog = opt_dialog(
      "Am I boang ???",
      ["Yes","No"], 
      [c1,c2]
    )
    
    NPC.new("Khert",a_view,dialog,SF.vector2(9.0,10.0))
  }


  DefaultView = ActorView.new(
    SpriteAnim.new(SageIdle,[18,18,18,18]),
    SpriteAnim.new(SageRi,  [18,18,18,18]),
    SpriteAnim.new(SageUp,  [18,18,18,18]),
    reg_emo([
      {ActorEmotion::Neutral, SgPortrait},
      {ActorEmotion::Wonder,  SgWonder},
      {ActorEmotion::Angry,   SgAngry}
      ]
    )
  )

end
