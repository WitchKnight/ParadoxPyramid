require "./Labels.cr"
require "./Stats.cr"
require "./Animation.cr"


module Classes

  include Labels
  extend self


  class ActorClass
    property name         : String
    property level        : Int32
    property cl_view      : Actors::ActorView
    property active       : Bool
    property description  : String
    property growth       : Proc(Int32,Array(LabelU))

    def initialize(@name,@level,@cl_view,@description,@growth)
      @active = true
    end

    def yes?
      return active && level >0
    end

    def toggle
      @active = !@active
    end
    
    def get_growth
      a = @growth.call(@level)
      return a
    end

  end  
  
  def make_class(name, requirements, items,sprite,description,growth)
    level = requirements.call(items)
    return Actors::ActorClass.new(name,level,sprite,description,growth)
  end

  def make_standard_reqs(weapons,armors)
    reqs = ->(items : Array(Item)){
      s = 0
      if wields(items, weapons) > 0
        s +=1
        s += wears(items, armors)*2
        p wears(items, armors) 
      end
      return s
    }
    return reqs
  end

  def load_class(file)
    cls = JSON.parse(File.read(file))
    name = cls["name"].as_s.to_s
    desc = cls["description"].as_s.to_s
    idletext  = SF::Texture.from_file("images/actors/" + cls["anim"]["idle" ][0].as_s.to_s)
    idleframedata = cls["anim"]["idle" ][1].as_a.map{|x|x.as_i.to_i}.to_a
    righttext = SF::Texture.from_file("images/actors/" + cls["anim"]["right"][0].as_s.to_s)
    rightframedata = cls["anim"]["right" ][1].as_a.map{|x|x.as_i.to_i}.to_a
    downtext  = SF::Texture.from_file("images/actors/" + cls["anim"]["down" ][0].as_s.to_s) 
    downframedata = cls["anim"]["down" ][1].as_a.map{|x|x.as_i.to_i}.to_a
    wps = cls["weapons"].as_a.map{ |w|
      to_label(w.as_s.to_s,0).class
    }
    arms = cls["armors"].as_a.map{ |w|
      to_label(w.as_s.to_s,0).class
    }

    growth = begin
      growths = [] of {String,Proc(Int32,Int32)}
      cls["growth"].as_a.each { |g|
        what = g["lid"].as_s.to_s
        rank = g["rank"].as_s.to_s
        gtype = Stats.gt_from_str(g["type"].as_s.to_s)
        gth = Stats.growth(rank,gtype)
        growths << {what,gth}
      }
      a = ->(class_level : Int32){
        return growths.map { |label, growth|
          to_label(label, growth.call (class_level))
        }
      }
      a
    end

    wps.each{|x| p x}
    arms.each{|x| p x}
    weapons = ->(w: Label){wps.includes? w.class}
    armors  = ->(w: Label){arms.includes? w.class}
    reqs    = make_standard_reqs(weapons,armors)

    a_view = Actors::ActorView.new(
      Animation::SpriteAnim.new(idletext,  idleframedata),
      Animation::SpriteAnim.new(righttext,  rightframedata),
      Animation::SpriteAnim.new(downtext,  downframedata),
      Init.reg_emo([
        {Actors::ActorEmotion::Neutral, Init::SgPortrait},
        {Actors::ActorEmotion::Wonder,  Init::SgWonder},
        {Actors::ActorEmotion::Angry,   Init::SgAngry}
      ])
    )

    return ->(items: Array(Item)){
      make_class(name,reqs,items, a_view,desc,growth)
    } 
  
  end

  AllLoadedClasses = Dir.entries("classes").select{|x|x.includes? ".json"}.map{ |x| 
    load_class("classes/"+x)
  }

  AllClasses = AllLoadedClasses

end
