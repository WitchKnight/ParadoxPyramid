
module Animation

  class SpriteAnim
    property texture    : SF::Texture
    property durations  : Array(Int32)
    property fliph       : Bool
    property flipv       : Bool
    
    def initialize(@texture,@durations)
      @fliph = false
      @flipv = false  
    end

    def initialize(@texture)
      @durations = [] of Int32
      @fliph = false
      @flipv = false  
    end

    def fliph!
      @fliph = !@fliph
    end

    def flipv!
      @flipv = !@flipv
    end
    
    def get_cur(time)
      if durations.any?
        frames = ( time.as_seconds * 60 ).to_i
        # we tie in the loop to the total number of frames of the animation
        frames = frames % @durations.sum

        # let's find out how far along we are in the animation
        index = 0
        (0...durations.size).each{|i|
          if @durations[0..i].sum >= frames
            index = i
            break
          end
        }

        # and let's make the sprite
        imgwidth = @texture.size.y // TSZ
        sprite = SF::Sprite.new(@texture)
        row = index//imgwidth
        col = index%imgwidth
        sprite.texture_rect =SF.int_rect(col*TSZ,row*TSZ,TSZ,TSZ)
        Base.center_sprite(sprite)

        sprite.scale(-1,1) if @fliph
        sprite.scale(1,-1) if @flipv

        return sprite
      else
        sprite = SF::Sprite.new(@texture)

        Base.center_sprite(sprite)

        sprite.scale(-1,1) if @fliph
        sprite.scale(1,-1) if @flipv

        return sprite
      end
    end
  end
end