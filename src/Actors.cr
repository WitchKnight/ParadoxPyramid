require "./Base.cr"
require "./Labels.cr"
require "./Classes.cr"
require "./Animation.cr"


module Actors
  include Animation
  include Labels
  include Classes
  extend self

  def talking_dist(act1,act2)
    return Base.get_diff(act1.position,act2.position) <= 1.1
  end

  enum ActorState
    Idle
    WUp
    WDown
    WLeft
    WRight
  end

  enum Faction
    Player
    Golem
    Dragon
    Alven
    Dwar
    Ryswori
    Olunrei
    Raei
    Neutral

    def color
      case self
      when Player
        return SF::Color.new(140,255,120)
      else
        return SF::Color.new(255,(100+self.to_i*20) % 255,120)
      end
    end
  end

  # class Stats

  alias ActorPortrait = Proc(Actor,SF::Sprite)

  enum ActorEmotion
    Wonder
    Angry
    Neutral
  end

  class Actor
    property name           : String
    property view           : ActorView= Init::DefaultView 
    property faction        : Faction  = Faction::Neutral
    property items          : Array(Item) = [] of Item
    property emotion        : ActorEmotion
    property state          : ActorState
    property position       : SF::Vector2(Float64)
    property labels         : Array(Label)
    property status         : Array(Label)
    property classes        : Array(Classes::ActorClass) = [] of Classes::ActorClass

    def confirm
      @labels = [] of Label
      @items.map{|a| @labels += a.qualities}
      build_classes
      act_classes
      sort_labels
    end


    def toggle_class(index)
      begin
        c = @classes[index]
        c.toggle
      rescue 
        puts "no such actor class"
      end
    end

    def build_identity
      male    =  Labels.getPw(@labels, ->(k: Label){k.is_a? Male}) > 4
      female  =  Labels.getPw(@labels, ->(k: Label){k.is_a? Female}) > 4
      tags = [] of String
      tags << "Male"   if male
      tags << "Female" if female
      return tags
    end

    def build_classes
      new_classes = AllClasses.map{|proc| proc.call(@items)}.select{|c|c.yes?}
      new_classes.each{|x|
        i = @classes.index{ |y| x.name == y.name} 
        if i.is_a? Int32
          c =  classes[i]
          x.active = c.active
        end
      }
      @classes  = new_classes
    end


    def act_classes
      @classes.each{|x|
        if x.active
          @labels += x.get_growth
          p x.level
        end
      }
    end

    def sort_labels
      uniques = begin
        seen = [] of Label
        @labels.each { |lab|
          names = seen.map{|x|x.label_name}
          if names.includes? lab.label_name
            index = names.index(lab.label_name)
            seen[index].power += lab.power if index
          else
            seen << lab
          end            
        }
        seen
      end
      @labels = uniques
    end

    def get_paradox
      return Labels.total_paradox(@labels+@status)
    end

    def stats
      sword_mastery = Labels.getPw(@labels, ->(k: Label){k.is_a? Sword})
      axe_mastery   = Labels.getPw(@labels, ->(k: Label){k.is_a? Axe})
      mace_mastery  = Labels.getPw(@labels, ->(k: Label){k.is_a? Mace})
      masteries = [sword_mastery,axe_mastery,mace_mastery]
      hp    = 10
      mp    = 10
      speed = 10
      return [masteries, hp, mp,speed]
    end


    def processTick(tick : Tick, scene)
      case tick.kind
      when TickType::EmptyTick
      when TickType::EventTick
        scene.processEvent(tick.name,tick.metadata)
      when TickType::ActionTick
        processAction(tick.name,tick.metadata)
      end
    end

    def processAction(actionName, actionData)
  
    end

    def equip(item)
      @items << item
      confirm
    end

    def unequip(item)
      @items -= [item]
      confirm
    end
    
    def get_portrait
      p = @view.portrait.call(self)
      return p
    end

    def initialize(@name,@view)
      @state = ActorState::Idle
      @emotion = ActorEmotion::Neutral
      @position= SF.vector2(0.0,0.0)
      @labels = [] of Labels::Label
      @status = [] of Labels::Label
    end

    def initialize(@name,@view,@faction)
      @state = ActorState::Idle
      @emotion = ActorEmotion::Neutral
      @position= SF.vector2(0.0,0.0)
      @labels = [] of Labels::Label
      @status = [] of Labels::Label
    end


    def initialize(@name,@view,@emotion,@items,@position,@labels,@status)
      @state = ActorState::Idle
    end


    def get_anim(time)
      cs =  @classes.select{|c|c.active}
      if cs.any?
        mc = cs.max_by{|x|x.level}
        vi = mc.cl_view
        return vi.get_anim(@state,time)
      else
        return @view.get_anim(@state,time)
      end
    end

    def get_anim_p(time)
      r = get_anim(time)
      r.scale = {2,2}
      return r
    end

    def info
      paradox
    end

    def paradox
      totpar = 0
      Labels::AllParadox.each{ |f|
        totpar += f.call(@labels)[0]
      }
      totpar += @classes.size**2
      return totpar
    end
  end

  class NPC < Actor
    property count  : Int32
    property dialog : Events::Dialog
    def initialize(@name,@view,@dialog,pos)
      super(@name,@view)
      @position = pos
      @count = 0
    end

    def to_act
      return Actor.new(@name,@view,@emotion,@items,@position,@labels,@status)
    end
  end


  alias HActors = Hash(Int32,Actor)

  class ActorView

    property idleLoop   : SpriteAnim
    property wrLoop     : SpriteAnim
    property wuLoop     : SpriteAnim
    property wdLoop     : SpriteAnim
    property wlLoop     : SpriteAnim
    property portrait   : ActorPortrait
    
    # battleLoop  : SF::Texture
    def initialize(@idleLoop,@wrLoop,@wdLoop,@portrait)
      @wlLoop = @wrLoop.dup
      @wlLoop.fliph!
      @wuLoop = @wdLoop.dup
      @wuLoop.fliph!
    end

    def get_anim(state,time)
      case state
      when ActorState::Idle
        idleLoop.get_cur(time)
      when ActorState::WDown
        wdLoop.get_cur(time)
      when ActorState::WRight
        wrLoop.get_cur(time)
      when ActorState::WUp
        wuLoop.get_cur(time)
      else
        wlLoop.get_cur(time)
      end
    end
    
  end


  enum TickType
    EmptyTick 
    InputTick
    PlanTick
    ActionTick
    EventTick
  end

  class Tick
    property kind : TickType
    property name : String
    property metadata : Metadata

    def initialize(@kind,@name,@metadata)
    end

end

  
end
