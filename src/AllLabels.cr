module AllLabels

  AllLabels = [
    None,


    Alfe,
    Dwarrow,
    Olunrei,
    Raei,
    Rysworan,

    Male,
    Female,
    Hermaphrodite,

    Head,
    Face,
    Neck,
    Chest,
    Back,
    Abdomen,
    LArm,
    RArm,
    LLeg,
    RLeg,
    LFoot,
    RFoot,

    Plastron,
    Plate,
    Light,

    Ring,
    Earring,
    Necklace,
    Amulet,

    Sword,
    Axe,
    Mace,
    Lance,
    Naginata,
    Bow,
    Throwing,
    Staff,
    Shield,

    Leather,
    Iron,
    Steel,
    Nacre,
    
    Ambidextry
  ]

end