require "yaml"
require "pars3k"



module Skills
  include Actors
  include Classes
  include Pars3k

  module Target
  end


  enum DamageType
    RandomType
    Piercing
    Slashing
    Crushing
    Burning
    Disease
    WeaponType
    UnknownType
    WorstType
    BestType
  end

  DoNothing = ->(game : GState,
                 actor: Actor) {}

  
  class FormulaResult 
    property values : Array({Float32,DamageType})

    def initialize(@values)
    end
    def initialize(value : {Float32,DamageType})
      @values = [value]
    end
    def +(f2 : FormulaResult)
      # check if there are compatible types
      f2.values.each do |fr|
        value, type = fr
        found = false
        self.values.each_with_index do |x,i|
          if x[1] == type && !found
            self.values[i] = x[0] + value
            found  = true
          end
        end
        # if there are none, we'll just add these values to the current formula result
        unless found
          @values << fr
        end 
      end
    end
    def -(f2 : FormulaResult)
      # check if there are compatible types
      f2.values.each do |fr|
        value, type = fr
        found = false
        self.values.each_with_index do |x,i|
          if x[1] == type && !found
            self.values[i] = x[0] - value
            found  = true
          end
        end
        # if there are none, we'll just add these values to the current formula result
        unless found
          @values << {-value, type}
        end 
      end
    end
    def *(f2 : FormulaResult)
      # check if there are compatible types
      f2.values.each do |fr|
        value, type = fr
        found = false
        self.values.each_with_index do |x,i|
          if (x[1] == type || type == UnknownType)  && !found
            self.values[i] = x[0] * value
            found  = true
          end
        end
        # if there are none, we'll just add these values to the current formula result
        unless found
          @values << fr
        end 
      end
    end
    def /(f2 : FormulaResult)
      # check if there are compatible types
      f2.values.each do |fr|
        value, type = fr
        found = false
        self.values.each_with_index do |x,i|
          if (x[1] == type || type == UnknownType)  && !found
            self.values[i] = x[0] / value
            found  = true
          end
        end
        # if there are none, we'll just add these values to the current formula result
        unless found
          @values << fr
        end 
      end
    end
    def of_type(t : DamageType)
      return FormulaResult.new({@values.map{|x| x[0]}.sum,t})
    end

  end

  abstract class FormulaToken
    def get_formula_result(game : GState) : FormulaResult
    end
  end

  class Plus < FormulaToken
    property f1 : FormulaToken
    property f2 : FormulaToken
    property sign : Bool
    def initialize(@f1,@f2,@sign)
    end
    def get_formula_result(game)
      if sign 
        return f1.get_formula_result(game) +f2.get_formula_result(game)
      else
        return f1.get_formula_result(game) -f2.get_formula_result(game)
      end
    end
  end

  class Times < FormulaToken
    property f1 : FormulaToken
    property f2 : FormulaToken
    def initialize(@f1,@f2)
    end
    def get_formula_result(game)
      return f1.get_formula_result(game) *f2.get_formula_result(game)
    end
  end

  class DivBy < FormulaToken
    property f1 : FormulaToken
    property f2 : FormulaToken
    def initialize(@f1,@f2)
    end
    def get_formula_result(game)
      return f1.get_formula_result(game) /f2.get_formula_result(game)
    end
  end

  class GetStat < FormulaToken
    property whose  : String
    property stat   : String
    property ratio  : Float32
    def initialize(@whose,@stat,@ratio)
    end
    def get_formula_result(game)
      actor = game.scene.get_actor(whose)
      if actor 
        statValue  = actor.get_stat(stat) 
        if stat
          return statValue*ratio
        end
      end
      return 0 
    end
  end

  class RecordedFormula < FormulaToken
    property formula_name : String
    
    def initialize(@formula_name)
    end

    def get_formula_result(game)
      formula = game.scene.battle_system.get_formula(formula_name)
      return formula.get_formula_result(game)
    end

  end
  
  class FlatValue < FormulaToken
    property value : Float32
    def initialize(@value)
    end
    def get_formula_result(game)
      return @value
    end
  end

  record NoToken
  alias FormulaParserResult = FormulaToken | NoToken
  class FormulaParser       
    property parser : Proc(String,FormulaParserResult)
    def initialize(@parser)
    end
    # the idea is to allow multiple parsers to try if one fails
    def |(other : FormulaParser)
      newProc = ->(string : String){
        result = begin
          r = self.parser.call(string)
          if r == NoToken
            r = other.parser.call(string)
          end
          return r
        end
      }
      return FormulaParser.new(newProc)
    end
  end
  TheFormulaParser =  TypeSpecifierParser |  DivParser | TimesParser | PlusParser | MinusParser | GetStatParser | FlatValueParser | RecordedFormulaParser

  TypeSpecifierParser = FormulaParser.new ->(input : String) {
    typeSpecifierChopped = input.split(" of ")
    if typeSpecifierChopped.size == 2
      raw_type = typeSpecifierChopped[1]
      type = DamageType.parse?(raw_type)
      if type 
        formula = TheFormulaParser.call(typeSpecifierChopped[0])
        if !(formula.is_a? NoToken)
          return TypeSpecifier.new(formula, type) 
        end
      end
    end
    return NoToken
  }



  RecordedFormulaParser = FormulaParser.new ->(input : String){
    if !input.includes_any?("+-*/")
      return RecordedFormula.new(input)
    end
    return NoToken
  }

  TilesParser = FormulaParser.new ->(input : String){
    firstOp = input.index('*')
    if firstOp
      raw_formula1 = input[0,firstOp]
      raw_formula2 = input[firstOp,-1]
      formula1 = TheFormulaParser.parse(raw_formula1)
      formula2 = TheFormulaParser.parse(raw_formula2)
      if formula1 != NoToken && formula2 != NoToken
        return Times.new(formula1,formula2)
      end
    end
    return NoToken
  }

  PlusParser = FormulaParser.new ->(input : String){
    firstOp = input.index('+')
    if firstOp
      raw_formula1 = input[0,firstOp]
      raw_formula2 = input[firstOp,-1]
      formula1 = TheFormulaParser.parse(raw_formula1)
      formula2 = TheFormulaParser.parse(raw_formula2)
      if formula1 != NoToken && formula2 != NoToken
        return Plus.new(formula1,formula2,true)
      end
    end
    return NoToken
  }

  MinusParser = FormulaParser.new ->(input : String){
    firstOp = input.index('-')
    if firstOp
      raw_formula1 = input[0,firstOp]
      raw_formula2 = input[firstOp,-1]
      formula1 = TheFormulaParser.parse(raw_formula1)
      formula2 = TheFormulaParser.parse(raw_formula2)
      if formula1 != NoToken && formula2 != NoToken
        return Plus.new(formula1,formula2,false)
      end
    end
    return NoToken
  }


  DivParser = FormulaParser.new ->(input : String){
    firstOp = input.index('/')
    if firstOp
      raw_formula1 = input[0,firstOp]
      raw_formula2 = input[firstOp,-1]
      formula1 = TheFormulaParser.parse(raw_formula1)
      formula2 = TheFormulaParser.parse(raw_formula2)
      if formula1 != NoToken && formula2 != NoToken
        return DivBy.new(formula1,formula2)
      end
    end
    return NoToken
  }


  
  class Formula
    property get_result : Proc(GState, Actor, Float32 | Int32 )
    def initialize(formulastring : String)
      formula = FormulaParser.parse(formulastring)
      @get_result = detokenize(formula)
    end
  end

  abstract class TargetType
  end

  struct SkillCost
    property fatigue  : Float32
    property tension  : Float32
    property mana     : Float32
    property custom   : Proc(GState,Actor,Nil)
    def initialize(@fatigue,@tension,@mana,@custom)
    end
    def initialize(@fatigue,@tension,@mana)
      @custom  = DoNothing
    end
  end


  class GroundTarget < TargetType
  end

  class CharacterTarget < TargetType
    property actorRef : Int32
    def initialize(@actorRef)
    end
  end

  class MobileTarget < TargetType
  end

  class Skill
    property name         : String
    property sdelay       : Int32
    property cooldown     : Int32
    property requirements : Hash(String,Int32)
    property cost         : SkillCost
    property model        : SkillModel?
    property targetting   : Targetting
    property tags         : Hash(String,Int32)

    def select_target(game : GState, actor : Actor)
      @targetting.wait_for_target_select(game, actor)
    end

    def initialize(@name,@sdelay,@cooldown,@model,@targetting,@cost,@requirements,@tags)
    end
    
    def initialize(@name,@sdelay,@cooldown,@model,@targetting,@cost,@requirements)
      @tags = {} of String => Int32
    end
  end

  abstract class Targetting
    def get_possible_targets(game : GState, actor : Actor)
    end
  end

  class OpenTargetting < Targetting
    property range : Int32
    property line_of_sight : Bool

    def initialize(@range, @line_of_sight)
    end

    def get_possible_targets(game : GState, actor : Actor) : Array(Coord)
      # get coord of the current player
      pos = game.scene.player.actor.position
      coords = [] of Coord
      (pos.x-range..pos.x+range).each do |i|
        (pos.y-range..pos.y+range).each do |j|
          coords << {i.to_i, j.to_i}
        end
      end
    end
  end

  module Interruptable
    def interrupt(game : GState, actor : Actor) : Nil
      on_interrupt.call(game, actor)
    end
  end


  abstract class SkillModel
    
    property on_start   : Proc(GState, Actor, Nil)
    property on_end     : Proc(GState, Actor, Nil)
    property formulas   : Hash(String, Formula)
    def initialize(@formulas)
      @on_start = DoNothing
      @on_end = DoNothing
    end

    def initialize(@on_start, @on_end,@formulas)
    end

    def start(game : GState, actor : Actor) : Nil
      on_start.call(game, actor)
    end


    def end(game : GState, actor : Actor) : Nil
      on_start.call(game, actor)
    end
    
  end

  class Charge < SkillModel

    include Interruptable
    property on_interrupt : Proc(GState, Actor, Nil)
    property on_hit       : Proc(GState, Actor, Nil)
    property target_type  : TargetType
    property time         : Int32
    property range        : Int32

    def initialize(on_start, on_end, @on_interrupt, @on_hit,@target_type, @time, @range)
      super(on_start, on_end)
    end

    
  end

  alias YamlData = String | Float32 | Int32 | Hash(String, YamlData)
  def load_cskills(actor)
    skills = AllClasses.each do |cls|
      dirname = "skills/class/"+cls.name
      Dir.entries(dirname).each do |file| 
        raw_cskill = File.read(dirname+'/'+file)
        cskill = Hash(String, YamlData).from_yaml raw_cskill
        sdelay      = cskill.fetch("delay",0.0).to_f
        name        = cskill.fetch("name",0.0).to_s
        cooldown    = cskill.fetch("cooldown",0.0).to_f
        cost        = cskill.fetch("cost",{} of String => YamlData).to_f
        req_level   = cskill.fetch("req_level",0).to_i
        skill_model = cskill.fetch("skill_model","none").to_s
        tags        = cskill.fetch("skill_tags").to_s
        raw_targetting = cskill.fetch("raw_targetting").to_s
        model = nil
        case skill_model
        when "charge"
          model =  Charge.new(on_start)
        end
        targetting = begin
          case raw_targetting
          when "open"
            return OpenTargetting.new(range,tags.includes?("nolos"))
          # when "slide 8"
          #   return LineTargetting.new(range,8)
          # when "slide 4"
          #   return LineTargetting.new(range,4)
          end         
        end
        Skill.new(name, sdelay,cooldown,model,targetting, requirements)
      end
    end
  end

  def make_charge(sdelay, cooldown, fatigue, req_level, skill)
    targetting  = cskill.fetch("targetting","tile").to_s
    move        = cskill.fetch("move","target").to_s
    speed       = cskill.fetch("speed",1).to_i
    range       = cskill.fetch("range",1).to_i

    prepare_skill = ->(game: GState,act: Actor, target:  {Int32,Int32}){
      final_skill = ->(game: GState){
        print("actor "+act.name+" is using a charge skill")
      }
      game.scene.prepare_skill(sdelay, final_skill)
    }

    targetting_skill = ->(game: GState,act: Actor){
      game.scene.choose_target(act,targetting,range, prepare_skill)
    }
  end

end