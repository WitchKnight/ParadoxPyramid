require "../lib/crsfml/*"

module Fonts
  

  MFont = SF::Font.from_file("fonts/menu.ttf")
  DFont = SF::Font.from_file("fonts/dragon.ttf")

  enum Font
    MenuFont
    DragonFont
    
    def get_text(string)
      case self
      when MenuFont
        thaFont = MFont
      when DragonFont
        thaFont = DFont
      end
      text = SF::Text.new
      if thaFont.is_a? SF::Font
        text.font = thaFont
        text.string = string
      end
      return text
    end
  end


end