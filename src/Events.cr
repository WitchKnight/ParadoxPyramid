module Events

  extend self
  
  alias DialogEvent = Proc(Scenes::RoamScene, Nil)

  class Dialog
    

    property selec      : Int32
    property line       : String
    property choices    : Array(String)
    property events     : Array(DialogEvent)


    def initialize(@line, @choices, @events)
      @selec = 0
    end


    def initialize(@selec, @line, @choices, @events)
    end

    def initialize(@line,event : DialogEvent)
      @selec = 0
      @choices = [] of String
      @events = events
    end

    def confirm(scene)
      @events[@selec].call(scene)
    end

    def blank
      return !choices.any?
    end

    def move(dir)
      a = @choices.size - 1
      case dir 
      when 2 
        @selec = (selec - 1).clamp(0,a)
      when 4
        @selec = (selec - 1).clamp(0,a)
      when 5
        @selec = selec
      else
        @selec = (selec + 1).clamp(0,a)
      end
    end

    def draw_choices(config,window)
      a = @choices.size
      @choices.each_with_index{|v,i|
        msg = v
        pos = { {(i+1),a+1}, {6,8} }
        t = Fonts::Font::MenuFont.get_text(msg)
        t.character_size = 36
        t.color = SF::Color.new(240,240,230)
        Base.center_text(t)
        t.position = Base.get_scr_pos(
          config,
          pos
        )
        window.draw(t)
      }
    end

    def draw_selector(config,window,time)
    
      selector_pic = SF::RectangleShape.new({12,12})
      selector_pic.origin = {6,6}
      selector_pic.position = Base.get_scr_pos(config,get_sel)
      selector_pic.move({-choices[@selec].size*12-10,0}) if @choices.any?
      selector_pic.rotate(time.as_milliseconds/10)
      window.draw(selector_pic)
    end



    def get_sel
      a = choices.size
      case a
      when 0
        return { {13,17},{6,8} }
      else
        return { {selec+1,a+1}, {6,8}}
      end
    end


    def get_pos
      a = choices.size
      return (0...a).map{ |x| { {(x+1),a+1}, {6,8} } }
    end

  end

  def blank_dialog(msg)
    return Dialog.new(msg, ([] of String), [Quit_Dialog])
  end

  def cont_with(func, dialog)
    c = ->(s: Scenes::RoamScene){
      func.call(s)
      s.change_dial dialog
      return nil
    }
    return c
  end


  def cont_dialog(dialog)
    c = ->(s: Scenes::RoamScene){
        s.change_dial dialog
        return nil
      }
    return c
  end

  def sim_dialog(msg)
    cont_dialog(blank_dialog(msg))
  end


  def chain_dialog(msg,dialog)
    return Dialog.new(msg, ([] of String), [cont_dialog(dialog)])
  end


  def opt_dialog(msg, choices, events)
    c = Dialog.new(msg, choices, events)
  end

  def ev_dialog(msg,event)
    return opt_dialog(msg,([] of String), [event] )
  end

  def chain_simple(msgs, finaldialog)
    uh = msgs.reverse
    dialog = finaldialog
    (0...uh.size).each{ |i| 
      msg = uh[i]
      dialog = chain_dialog(msg, dialog)
    }
    return cont_dialog(dialog)
  end

  def just_text(msgs)
    uh = msgs.reverse
    dialog = blank_dialog uh[0]
    (1...uh.size).each{ |i| 
      msg = uh[i]
      dialog = chain_dialog(msg, dialog)
    }
    return cont_dialog(dialog)
  end

  def emo(emotion)
    c = ->(s: Scenes::RoamScene){
        s.npcs[s.cnpc].emotion = emotion
        return nil
    }
  end

  #PREMADE EVENTS

  Quit_Dialog = ->(s : Scenes::RoamScene){
    s.quit_dialog
    return nil
  }

  Provoke_Battle = ->(s : Scenes::RoamScene){
    s.aggro([s.cnpc])
    return nil
  }









end