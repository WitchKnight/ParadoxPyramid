require "./*"


alias SMen = Scenes::MenuScene
alias SMElem = Scenes::MenuElem
alias Conf = Config::Config
alias Ft = Fonts::Font
alias ISMen = InGame::IGMenuScene
module Game
  include Input
  alias Conf = Config::Config
  class GameState
    property config = Conf.new
    property window       : SF::RenderWindow
    property scene        : Scenes::Scene = Init::ZeroScene.call
    property inputs       : GInputs = GInputs.new
    property cds          : Input::Cooldowns 
    property soundBuffers : Hash(String,SF::SoundBuffer) = Init::AllSounds
    property sounds       : Array(SF::Sound) = [] of SF::Sound
    def initialize(@window)
      @cds= Input::Cooldowns.new(@inputs)
      @scene = SMen.new(Init::MainMenu,self)
      @soundBuffers
      @sounds 
    end

    def update(dt)
      @scene.update(self,dt)
    end

    def draw
      @scene.draw(config,window)
    end

    def handleEvents(event,window)
      case event
      when SF::Event::Closed
        window.close
      when SF::Event::KeyPressed
        c = event.code
        if inputs.trigg(Menu_moves::Up,c)
          self.scene.dir_move(8)
        elsif inputs.trigg(Menu_moves::Down,c)
          self.scene.dir_move(2)
        elsif inputs.trigg(Menu_moves::Left,c)
          self.scene.dir_move(4)
        elsif inputs.trigg(Menu_moves::Right,c)
          self.scene.dir_move(6)
        end
      when SF::Event::KeyReleased
        c = event.code
        if inputs.trigg(Menu_moves::Up,c)
          self.scene.dir_stop(8)
        elsif inputs.trigg(Menu_moves::Down,c)
          self.scene.dir_stop(2)
        elsif inputs.trigg(Menu_moves::Left,c)
          self.scene.dir_stop(4)
        elsif inputs.trigg(Menu_moves::Right,c)
          self.scene.dir_stop(6)
        elsif inputs.trigg(Uni_moves::Confirm,c)
          self.scene.user_confirm(self)
        elsif inputs.trigg(Uni_moves::Cancel,c)
          self.scene.cancel(self)

        end
      when SF::Event::MouseButtonReleased
        c = event.button
        if c == SF::Mouse::Button::Left
          self.scene.user_confirm(self)
        elsif c == SF::Mouse::Button::Right
          self.scene.cancel(self)
        end
      end
    end

    def reset_scene(scene)
      scene.time = SF.seconds(0)
      @scene = scene
      return nil
    end

    def switch_scene(scene)
      @scene = scene
      return nil
    end

    def switch_menu(menu)
      self.switch_scene(SMen.new(menu,self))
      return nil
    end

    def switch_ig_menu(menu,scene)
      self.switch_scene(ISMen.new(menu,scene,self))
      return nil
    end

    def refresh
      s = @scene

      if s.is_a? ISMen
        s.rebuild
      end
      self.inputs.reload
    end

    def cancel
      @scene.cancel(self)
    end
 
    def quit
      exit(0)
      return nil
    end

    def draw(window)
      @scene.draw(@config,window)
    end

    def play_sound(name)
      sound = SF::Sound.new(soundBuffers[name])
      sounds << sound
      sound.play
    end

    def stop_sounds()
      sounds.each_with_index do |i,sound|
        sound.stop
        sounds.delete_at(i)
      end
    end

  end
end