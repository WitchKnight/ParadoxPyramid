
module Player
  include Actors
  include Labels

  class Player
    property name       : String
    property actor      : Actor
    property inventory  : Hash(Coord,Item)
    
    def initialize(@name,@actor)
      @inventory = {} of Coord => Item
    end

    def main_sprite(time)
      return actor.get_anim(time)
    end

    def toggle_class(index)
      @actor.toggle_class(index)
    end

    def handle_turn(dt,dir)
    end
  end

 
end
