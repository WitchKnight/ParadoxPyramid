require "json"
require "./Animation.cr"


module ItemInit
  include Labels
  extend self

  def load_weapon(file)
    wp = JSON.parse(File.read(file))
    name = wp["name"].as_s.to_s
    desc = wp["description"].as_s.to_s
    usb = wp["usable"]
    
    types = wp["types"].as_a.map { |x|
      to_label(x["lid"].as_s.to_s,x["lpow"].as_i.to_i)
    }
    labels = wp["labels"].as_a.map { |x|
      to_label(x["lid"].as_s.to_s,x["lpow"].as_i.to_i)
    }
    weight  = wp["weight"].as_i.to_i
    power   = wp["power"].as_i.to_i

    damage  = wp["damage"].as_i.to_i 
    parade  = wp["parade"].as_i.to_i
    crit    = wp["crit"].as_i.to_i
    speed   = wp["speed"].as_i.to_i
    range   = wp["range"].as_i.to_i
    
    head_type = types[0]

    anim_texture = head_type.label_texture
    anim = SpriteAnim.new(anim_texture)


    return Weapon.new(
      power,
      name,
      labels,
      weight, 
      desc,
      anim,
      types, 
      nil,
      damage,
      parade,
      crit,
      speed,
      range
    )
  end



  def load_armor(file)
    wp = JSON.parse(File.read(file))
    name = wp["name"].as_s.to_s
    desc = wp["description"].as_s.to_s
    usb = wp["usable"]

    types = wp["types"].as_a.map { |x|
      to_label(x["lid"].as_s.to_s,x["lpow"].as_i.to_i)
    }

    materials = wp["materials"].as_a.map { |x|
      to_label(x["lid"].as_s.to_s,x["lpow"].as_i.to_i)
    }

    coverage = wp["coverage"].as_a.map { |x|
      to_label(x.as_s.to_s, 0)
    }


    labels = wp["labels"].as_a.map { |x|
      to_label(x["lid"].as_s.to_s,x["lpow"].as_i.to_i)
    }

    defense = wp["defense"].as_i.to_i 


    weight = wp["weight"].as_i.to_i
    power  = wp["power"].as_i.to_i
    head_type = types[0]
    
    anim_texture = head_type.label_texture
    anim = SpriteAnim.new(anim_texture)

    return Armor.new(
      power,
      name, 
      labels,
      weight,
      desc,
      anim,
      types, 
      nil,
      materials, 
      coverage, 
      defense
    )
  end


  Weapons = Dir.entries("items/weapons").select{|x|x.includes? ".json"}.map { |x|
    load_weapon("items/weapons/"+x)
  }

  Armors = Dir.entries("items/armors").select{|x|x.includes? ".json"}.map { |x|
    load_armor("items/armors/"+x)
  }

  Weapons.each {|x| print x.to_s}
  Armors.each {|x| print x.to_s}
end
