require "./Game.cr"
module Sound
  extend self

  include Base

  StandardSoundTable = {
    "Confirm" => "confirm0",
    "Cancel"  => "cancel0",
    "Step"    => "step0"
  }



class Music
  property audio  : SF::Music?
  property name   : String


  def initialize(@audio, @name)
  end

  def initialize( @name)
  end

  def play()
    audio?.play()
  end

  def stop()
    audio?.stop()
  end

end

class SoundSystem(Game)
  property table : Hash(String,String)


  def initialize(@table)
  end

  def initialize
    @table = StandardSoundTable
  end

  def play(game : Game,name : String)
    game.play_sound(table[name])
  end

  def stop(game : Game,name : String)
    game.stop_sound(table[name])
  end
end

end