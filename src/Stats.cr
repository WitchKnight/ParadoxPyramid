module Stats
  extend self

  enum Rank
    Common    #
    Brave     # 
    Heroic    # 
    Legendary #Primas,Nathanel
    Divine    #Seigneurs Dragons, Avatars Divins, Grands Mystiques, Manifestations, Parachevés
    Cosmic    #T,D
  end

  enum GrowthType
    Line
    Expo
    Loga
    Fixd

    

  end

  def gt_from_str(str)
    p "nodqs"
    p str.capitalize
    case str.capitalize
    when "Line"
      return GrowthType::Line
    when "Expo"
      return GrowthType::Expo
    when "Loga"
      return GrowthType::Loga
    else 
      return GrowthType::Fixd
    end
  end










  def growth(grade,gtype)
    coeff = begin
      case grade  
      when "X"
        b = 1.2
      when "S"
        b = 1.1
      when "A"
        b = 1
      when "B"
        b = 0.8
      when "C"
        b = 0.6
      else
        b = 0.5
      end
      b
    end

    case gtype
    when  GrowthType::Line
      return ->(class_level : Int32){
        return (coeff*class_level).round.to_i
      }
    when  GrowthType::Expo
      return ->(class_level : Int32){
        return (coeff*(1.2**class_level.to_f)).round.to_i
      }
    when  GrowthType::Loga
      return ->(class_level : Int32){
        return (coeff*5*(Math.log(class_level.to_f))).round.to_i
      }
    else
      return ->(class_level : Int32){
        return (10*coeff**2).round.to_i
      }
    end
  end


end