require "./Game.cr"
require "./Actors.cr"
require "./Config.cr"
require "./Sound.cr"
require "./Player.cr"
require "./World.cr"
require "./Input.cr"
require "./Battle.cr"
alias GState = Game::GameState


require "../lib/crsfml/src/*"
require "../lib/crsfml/src/audio/*"


module Scenes
  include Input
  include Actors
  include Battle
  extend self
 

  alias SoundSystem = Sound::SoundSystem(GState)
  alias Ft = Fonts::Font
  alias Crd       = {Int32,Int32}
  alias MenuCoord = { Crd, Crd }
  alias Action    = Nil | Proc(GState,Nil) | Proc(Nil)
  alias Label     = NamedTuple(centered: Bool,text: String,font: Ft,character_size: Int32,color: SF::Color)
  alias MayAnim = Nil | Proc(SF::Time,Array(SF::Sprite)) 

  def display_description(text)
    text2 = text.split(" ")
    cc = 0
    lines = [] of String
    line = [] of String
    (0...text2.size).each do |i|
      word = text2[i]
      cc += word.size
      line << word
      unless cc <= 20
        lines << line.join(" ")
        line = [] of String
        cc = 0
      end
    end
    lines << line.join(" ")
    final = lines.join("\n")
    return final
  end

  
  def mk_std_label(text,size) 
    {
      text: text,
      centered: false,
      font:  Ft::MenuFont,
      character_size: size,
      color: SF::Color::White
    }
  end

  def mk_std_label(text,size,color) 
    {
      text: text,
      centered: false,
      font:  Ft::MenuFont,
      character_size: size,
      color: color
    }
  end


  def mk_big_label(text) 
    mk_std_label(text,40)
  end
  
  BlankLabel = mk_big_label("")

  struct MenuElem
    property label        : Label
    property cancelac     : Bool = false
    property active       : Bool  = false
    property inapic       : MayAnim
    property apic         : MayAnim
    property description  : String | Nil
    property action       : Action
    property threshold    : Float64 = 0.08

    def initialize(@label,@active,@inapic,@apic,@description,@action)
    end

    def initialize(@label,@active,@inapic,@description,@action)
    end

    def initialize(@label,@active,@description,@action)
    end

    def cancel
      a = MenuElem.new(@label,@active,@inapic,@apic,@description,@action)
      a.cancel!
      return a
    end

    def activated
      a = self
      a.active = true
      return a
    end

    def centered
      a = self
      l =  { 
        text: @label[:text],
        centered: true,
        font: @label[:font],
        character_size: @label[:character_size],
        color: @label[:color]
      }
      a.label = l
      return a
    end

    def threshold(threshold)
      a = self.dup
      a.threshold = threshold
      return a
    end


    def cancel!
      @cancelac = true
    end

    def draw_hl(time,config, window, mpos : MenuCoord)
      position = Base.get_scr_pos(config, mpos)
      dapic = @apic
      if dapic
        pics = dapic.call(time)
        pics.each{ |pic|
          pic.position = position
          window.draw(pic)
        }
      end
    end

    def draw_at(time,config, window, mpos : MenuCoord)
      position = Base.get_scr_pos(config, mpos)
      dipic = @inapic
      if dipic
        pics = dipic.call(time)
        pics.each{ |pic|
          pic.position = position
          window.draw(pic)
        }
      end

      label_text = @label[:font].get_text(@label[:text])
      if label_text.is_a? SF::Text
        label_text.character_size = @label[:character_size]
        label_text.color = @label[:color]
        Base.center_text(label_text)
        Base.left_text(label_text) unless @label[:centered]
        label_text.position = position
        window.draw(label_text)
      end
    end

  end

  struct Menu
    property elements       : Hash(MenuCoord,MenuElem)
    property default        : MenuCoord
    property music          : Sound::Music
    property extra          : Proc(MenuCoord,Hash(MenuCoord,MenuElem))

    def initialize(@elements ,@default )
      @extra = ->(c: MenuCoord){ {} of MenuCoord => MenuElem }
      @music = Sound::Music.new("noMusic")
    end

    def initialize(@elements ,@default,  @extra)
      @music = Sound::Music.new("noMusic")
    end

    def initialize(@elements ,@default , @music, @extra)
    end
  end

   class Scene
    property time : SF::Time 
    property game : GState?
    property timescale : Float64
    property sounds : SoundSystem

    def initialize()
      @time = SF.milliseconds(0)
      @timescale = 1.0
      @sounds = SoundSystem.new()
    end
    def initialize(@game)
      @time = SF.milliseconds(0)
      @timescale = 1.0
      @sounds = SoundSystem.new()
    end

    def initialize(@time,@timescale,@game)
      @sounds = SoundSystem.new()
    end

    def initialize(@time,@timescale,@sounds,@game)
    end

    def play_music()
    end


    def stop_music()
    end

    def cancel(game)
    end
    
    def dir_move(direction)
    end

    def dir_stop(direction)
    end

    def user_confirm(game)
    end

    def draw(window, config)
    end
    def update(game,dt)
      @time += dt*@timescale
    end

  end

  # class NoScene < Scene
  # end

  class MenuScene < Scene
    property menu     : Menu
    property selector : MenuCoord
    property view     : SF::View
    property v_init   : Bool = false


    def initialize(menux,game)
      super(game)
      @selector = menux.default
      @menu     = menux
      @view = SF::View.new
    end

    def update(game,dt)
      super
      mpos = SF::Mouse.get_position(game.window)
      menumpos = Base.convert_mouse(mpos,game.config.size)
      mouseover = false
      @menu.elements.each { |k,v| 
        if Base.get_distance(k,menumpos) < v.threshold && v.active
          @selector = k
          mouseover = true
          break
        end
      }
      # user_confirm(game) if SF::Mouse.button_pressed?(SF::Mouse::Button::Left) && mouseover
    end


    def mk_view(config,window)
      view = SF::View.new
      view.viewport  = SF.float_rect(0,0,1,1)
      view.size      = SF.vector2(config.size[0],config.size[1])
      view.center    = {config.size[0]/2,config.size[1]/2}
      @view = view
      @v_init = true
      window.view = @view 
    end


    def play_music()
      @menu.music.play()
    end

    def stop_music()
      @menu.music.stop()
    end

    def draw(config, window)
      mk_view(config,window) unless v_init
      @menu.elements.each do | position, element |
        element.draw_at(@time,config,window,position) unless position == selector && element.apic && element.label==BlankLabel
      end

      highlighted = @menu.elements[@selector]
      highlighted.draw_hl(@time,config,window,@selector)

      unless highlighted.apic || !highlighted.active 
        selector_pic = SF::RectangleShape.new({12,12})
        selector_pic.origin = {6,6}
        selector_pic.position = Base.get_scr_pos(config,@selector)
        selector_pic.move({-40,0})
        selector_pic.rotate(@time.as_milliseconds/10)
        window.draw(selector_pic)
      end

      @menu.extra.call(@selector).each do | position, element |
        element.draw_at(@time,config,window,position)
      end

    end

    def dir_move(direction)
      if g = @game
        @sounds.play(g, "Step")
      end
      active_elements = @menu.elements.select {|k,v| k != @selector && v.active == true && Base.is_in_dir(@selector,k,direction) }
      if !active_elements.empty?
        distances = {} of Float64 => MenuCoord
        active_elements.each_key.each do |x| 
          newone = Base.get_distance(x,@selector)
          distances = distances.merge({ newone => x })
        end
        elect = distances.each_key.min
        @selector = distances[elect]
      end
    end
    def user_confirm(game)
      @sounds.play(game, "Confirm")
      action = @menu.elements[@selector].action
      if action.is_a? Proc(GState,Nil)
        action.call(game)
      elsif action.is_a? (Proc(Nil))
        action.call
      end
    end

    def cancel(game)
      @sounds.play(game, "Cancel")
      actions = @menu.elements.select{ |k,v| v.cancelac}.map{|k,v| [k,v.action]}
      if actions.any?
        action = actions[0][1]
        if action.is_a? Proc(GState,Nil)
          action.call(game)
        elsif action.is_a? (Proc(Nil))
          action.call
        end
      end
    end

  end

  

  class CineScene < Scene
    property slides     : Hash(Float64,Proc({Config::Config,SF::RenderWindow,SF::Time},Nil))
    property next_scene : Scene | CineScene | RoamScene | MenuScene
    property zone       : World::Zone | Nil


    def initialize(slides, next_scene)
      super()
      @slides = slides
      @next_scene = next_scene
      c = @next_scene
      if c.is_a? RoamScene
        @zone = c.zone
      end
    end

    def initialize(slides, next_scene,game)
      super(game)
      @slides = slides
      @next_scene = next_scene
      c = @next_scene
      if c.is_a? RoamScene
        @zone = c.zone
      end
    end
    def draw(config,window)
      u = @slides.each_key.select{|k| k >= @time.as_seconds }.to_a.any?
      if u
        k =@slides.each_key.select{|key| key > @time.as_seconds}.min
        ct=@slides.each_key.select{|key| key < k}.sum
        a=@slides[k]
        a.call({config,window,@time - SF.seconds(ct)})
      end
    end

    def update(game,dt)
      super
      if @slides.each_key.select{|k| k >= @time.as_seconds }.to_a.empty?
        game.switch_scene(@next_scene)
      end
    end

    def user_confirm(game)
      @slides =@slides.select{|k,v| k != @slides.each_key.min}
    end

  end


  class RoamScene < Scene
    property zone     : World::Zone
    property player   : Player::Player
    property camera   : Array(Float64)
    property cam_z    : Float64 
    property cnpc     : Int32
    property npcs     : Array(NPC)

    property dialview : SF::View = SF::View.new
    property dialinit : Bool = false
    property dialrect : SF::RectangleShape = SF::RectangleShape.new
    property portrect : SF::RectangleShape = SF::RectangleShape.new

    property mapview  : SF::View = SF::View.new
    property mapinit  : Bool = false
    property bview_init : Bool = false

    

    def initialize(@zone,@player,@npcs,game)
      super(game)
      @camera = [@zone.start[0].to_f,@zone.start[1].to_f]
      @cam_z  = 2.0
      @cnpc = -1
      rplay.position = SF.vector2( @zone.start[0].to_f, @zone.start[1].to_f)
    end

    def initialize( otherscene)
      super(otherscene.time,otherscene.timescale,otherscene.game)
      @zone = otherscene.zone
      @player = otherscene.player 
      @camera = otherscene.camera 
      @cam_z = otherscene.cam_z 
      @cnpc = otherscene.cnpc 
      @npcs = otherscene.npcs 
      @dialview = otherscene.dialview 
      @dialinit = otherscene.dialinit 
      @dialrect = otherscene.dialrect 
      @portrect = otherscene.portrect 
      @mapview = otherscene.mapview 
      @mapinit = otherscene.mapinit
    end
    
    def rplay
      @player.actor
    end

    def makedialView(config,window)
      view = SF::View.new
      view.viewport  = SF.float_rect(0.1,0.1,0.8,0.8)
      view.size      = SF.vector2(config.size[0]*0.8,config.size[1]*0.8)
      view.center    = config.center
      r = SF::RectangleShape.new
      r.size = SF.vector2(700,200)
      r.outline_color = SF::Color.new(255,240,220)
      r.outline_thickness = 14
      r.fill_color = SF::Color::Black
      Base.center_rect(r)
      r.position = Base.get_scr_pos(
        config,
        { {1,2},{2,3} })
      @dialrect = r
      r = SF::RectangleShape.new
      r.size = SF.vector2(90,90)
      r.outline_color = SF::Color.new(255,240,220)
      r.outline_thickness = 14
      r.fill_color = SF::Color::Black
      Base.center_rect(r)
      r.position = Base.get_scr_pos(
        config,
        { {1,4},{3,7} })
      @portrect = r
      @dialview = view
      @dialinit = true
    end

    def makemapview(config,window)
      view = SF::View.new
      view.viewport  = SF.float_rect(0.1,0.1,0.8,0.8)
      view.size      = SF.vector2(config.size[0]*0.8,config.size[1]*0.8)
      view.zoom(1.fdiv @cam_z)
      @mapview = view
      @mapinit = true
    end

    def update(game,dt)
      super
      handle_mouse(game,dt)
      handle_cam(dt)
      handle_player(game,dt)
      update_battle(game,dt)
      game.cds.update(dt)
    end
    
    def handle_player(game,dt)
      dirs = game.inputs.get_map_dir
      dirs.each {|dir|
        roam(dt,dir)
      }
      rplay.state = ActorState::Idle if dirs.empty?
    end

    def roam(dt,dir)
      r =  dt.as_seconds*3
      case dir
      when 2
        rplay.state = ActorState::WDown
      when 4
        rplay.state = ActorState::WLeft
      when 6
        rplay.state = ActorState::WRight
      when 8
        rplay.state = ActorState::WUp
      else
        rplay.state = ActorState::Idle
      end
      newpos = attempt_movement(dir,r)
      res    = Base.snap(newpos)
      @cnpc = meet_npc if newpos != rplay.position && eng_actor
      rplay.position = newpos if @zone.move?(res) 
    end

    def attempt_movement(dir,r)
      pos = rplay.position
      case dir
      when 2
        pos += SF.vector2(0.0,r)
      when 4
        pos -= SF.vector2(r,0.0)
      when 6
        pos += SF.vector2(r,0.0)
      when 8
        pos -= SF.vector2(0.0,r)
      end
      return pos
    end

    def current_pos
      return rplay.position
    end

    def snap_pos
      nx = player.actor.position[0].to_f + temp_pos.x
      ny = player.actor.position[1].to_f + temp_pos.y
      nc = {nx.round.to_i,ny.round.to_i}
      return nc
    end

    def draw_mini_map(config, window)
      view = SF::View.new
      view.viewport   = SF.float_rect(0.75,0.75,0.20,0.20)
      view.center     = SF.vector2(camera[0],camera[1])
      view.size       = SF.vector2(config.size[0]*0.2,config.size[1]*0.2)
      window.view     = view
      m = @zone.display_mini
      m.each { |x|
        window.draw(x)
      }
    end

    def aggro(indexes)
    end

    def eng_actor
      cnpc != -1
    end

    def close_actor
      meet_npc != -1
    end

    def cancel(game)
      if cnpc != -1
        quit_dialog
      else
        game.switch_scene(InGame::IGMenuScene.new(Init::BuildIGMenu,self,game))
      end
    end

    def quit_dialog()
      @cnpc = -1
    end

    def draw_dialog(config,window)
      makedialView(config,window) if !dialinit
      window.view    = dialview
      if eng_actor
        #actor
        x = npcs[cnpc]
        # drawing the dialog box
       
        window.draw(@dialrect)

        # drawing the portrait box

        window.draw(@portrect)


        # drawing the npc portrait
        portrait = x.get_portrait
        Base.center_sprite(portrait)
        portrait.position = Base.get_scr_pos(
          config,
          { {1,4},{3,7} }
        )
        window.draw(portrait)

        # drawing the dialog itself
        c = x.dialog
        t = Fonts::Font::MenuFont.get_text(x.name + " : " +c.line)
        t.character_size = 38
        t.color = SF::Color::Red
        Base.center_text(t)
        t.position = Base.get_scr_pos(
          config,
          { {1,2},{4,7} })
        window.draw(t)



        c.draw_choices(config,window)
        c.draw_selector(config,window,@time)
      end
    end

    def draw_player(config,window)
      sp = rplay.get_anim(@time)
      apos = current_pos
      sp.position ={apos[0]*TSZ, apos[1]*TSZ}
      window.draw(sp)
    end

    def draw(config,window)
      if !mapinit
        makemapview(config,window) 
      else
        window.view = @mapview 
        m = @zone.display
        m.each { |x|
          window.draw(x)
        }
        draw_npcs(config,window) 
        draw_player(config,window) 
        draw_map_extras(config,window)
        draw_dialog(config,window) 
        draw_mini_map(config,window)
        draw_over_extras(config,window)
      end
    end

    def draw_over_extras(config,window)
    end

    def draw_map_extras(config,window)
    end

    def draw_npcs(config,window)
      @npcs.each{|npc|
        sp = npc.get_anim(@time)
        apos = npc.position
        sp.position ={apos[0]*TSZ, apos[1]*TSZ}
        window.draw(sp)
      }      
    end

    def meet_npc
      x = -1
      @npcs.each_with_index{ |npc,i|
        x = i if Actors.talking_dist(rplay,npc)
        break if Actors.talking_dist(rplay,npc)
      }
      return x
    end
  
    def handle_cam(dt)
      @camera = current_pos.to_a
      @mapview.center    = SF.vector2(camera[0]*TSZ,camera[1]*TSZ)
    end

    def handle_mouse(game,dt)
      mpos = SF::Mouse.get_position(game.window)
      menumpos = Base.convert_mouse(mpos,game.config.size)
      if eng_actor
        dialog = npcs[cnpc].dialog
        cpos = dialog.get_pos
        cpos.each_with_index { |k,i|
          if Base.get_distance(k,menumpos) < 0.1
            dialog.selec = i
            break
          end
        }
      end
    end

    def dir_move(dir)
      c = npcs[@cnpc]
      c.dialog.move(dir) if c
    end

    def user_confirm(game)
      d = npcs[cnpc]
      if eng_actor
        d.dialog.confirm(self)
      elsif close_actor
        @cnpc = meet_npc
      end
    end

    def change_dial(dialog)
      c = npcs[@cnpc]
      c.dialog = dialog
    end

    def update_battle(game,dt)
      trigger =  game.cds.trigger(Map_moves::Battle,1.0)
      game.scene=switch_battle if trigger
    end

    def switch_battle
      return BattleScene.new(self)
    end

  end


  class BattleScene < RoamScene
    property battleview   : SF::View
    property bview_init   : Bool = false
    property battle_phase : Hash(Actor, BattleActorState)
    property on_turn      : Bool = false


    def processEvent(eventName,metadata)
    end

    def initialize(roam)
      super(roam)
      @battleview = SF::View.new
      @battle_phase = {} of Actor => BattleActorState
      @npcs.each { |c|
        @battle_phase[c] =  BattleActorState.new()
      }
      @battle_phase[@player.actor] = BattleActorState.new()
    end



    def update_battle(game,dt)
      @battle_phase.each{ |k,v|
        v.update(dt)
      }
      actortick = @battle_phase[player.actor].get_tick()
      if actortick && actortick.kind != TickType::InputTick
        tick_battle(game,dt)
      end
    end

    def tick_battle(game,dt)
      @battle_phase.each { |k,v|
        tick = v.get_tick()
        if tick
          k.processTick(tick, self)
        end
      } 
    end

    def handle_player(game,dt)
      dirs = game.inputs.get_map_dir
      dirs.each {|dir|
        player.handle_turn(dt,dir)
      }
    end
    
    def init_bview(config)
      @battleview.size       = SF.vector2(config.size[0],config.size[1])
      @bview_init= true
    end

    def draw_map_extras(config,window)
      super
      
      init_bview(config) unless bview_init
      
      window.view = @battleview
      t = Fonts::Font::MenuFont.get_text("BATTLE")
      t.character_size = 35
      t.color = SF::Color::Red
      Base.center_text(t)
      t.position = Base.get_scr_pos(
        config,
        { {1,2},{1,2} })
      window.draw(t)
    end

    def switch_battle
      return RoamScene.new(self)
    end


  end



end
