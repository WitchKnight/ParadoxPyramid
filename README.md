# Paradox



## Installation

If you're on windows,
Dual Boot , use a VM or WSL.
for other platforms :

1. install SFML     https://www.sfml-dev.org/
2. install crystal  https://crystal-lang.org/
3. install libyaml-dev 
4. install crsfml   https://github.com/oprypin/crsfml using the method 1.

Just "crystal  /src/Paradox.cr --release" once you have both crystal and crsfml installed. dont forget the LIBRARY_PATH and LD_LIBRARY_PATH variables

-- How to get WSL working on windows

1. Enable WSL : https://winaero.com/blog/enable-wsl-windows-10-fall-creators-update/
2. Install Latest version of Ubuntu for WSL on the windows store (or your OS of choice) 
3. Download and install Xming : https://sourceforge.net/projects/xming/
4. Follow instructions above for other platforms. Here are the steps for ubuntu:
5. sudo apt install git cmake g++ libyaml-dev libsfml-dev 
6. install crystal normally and jump to 10 OR if it tells you the key is not signed: Either fix it properly if you know how or just run the following commands (yes it is very dirty):
7. sudo echo "deb  [allow-insecure=yes] https://dist.crystal-lang.org/apt crystal main" > /etc/apt/sources.list.d/crystal.list
8. sudo apt update 
9. sudo apt install crystal --allow-unauthenticated
10. install crsfml using the method 1 as described in the github page
11. Launch Xming in windows
12. run "export  DISPLAY=:0" in the WSL terminal
13.Compile the project and run the game

I have verified this method works on Windows 10 Home.

## Usage

Paradox is a RPG where you don't really know who you are and neither does the game.

As you use skills, act a certain way, use items and wear some pieces of equipment, you "mantle" some kind of identity..

But if you act in too many contradictory ways... Are you sure you can handle the paradox ?

## Development

Infancy

## Contributing

I am encouraging bug fixing and optimization suggestions.
Keep in mind : this is a learning project. I'd rather reinvent the wheel and fail than use a tried and true method with a black box.

## Contributors

- Aldeas  - cocreator
- Xam     - cocreator

## Other
This project is part of the Teddy Project.

