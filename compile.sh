
compilargs="";

if [[ $1 =~ "w" ]] ; then
    compilargs="--cross-compile -Dgc_none --target x86_64-pc-windows-msvc";
fi




if ! [[ $1 =~ "u" ]] ; then
   LIBRARY_PATH=./libs:/usr/local/lib:/home/wavesmith/build/crsfml/voidcsfml LD_LIBRARY_PATH=$LIBRARY_PATH  crystal build $compilargs src/Paradox.cr;
fi


if [[ $1 =~ "r" ]] ; then
    LIBRARY_PATH=./libs:/usr/local/lib:/home/wavesmith/build/crsfml/voidcsfml LD_LIBRARY_PATH=$LIBRARY_PATH  crystal run --verbose src/Paradox.cr ;
fi

